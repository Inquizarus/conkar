<?php

include SYSPATH . 'Helpers' . DS . 'functions' . EXT;
include SYSPATH . 'Helpers' . DS . 'autoload' . EXT;

date_default_timezone_set('Europe/Stockholm');
setlocale(LC_ALL, 'en_GB');

define('BASE_DIR', 'conkar');

if (!isCli()) {


    Conkar::$envMode = Conkar::DEVELOPMENT;
    $application = new Conkar;

    spl_autoload_unregister('autoloadClasses');
    spl_autoload_register(function ($class) use ($application) {
        call_user_func_array('autoloadClasses', [$class, $application]);
    });
    $application->run();
}
