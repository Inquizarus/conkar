<?php
/* Store folder names */
$application  = 'application';
$system 	  = 'system';
$extensions   = 'extensions';
$storage      = 'storage';

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR);
define('DOCROOT', dirname(__FILE__));
define('EXT', '.php');

/* Set error reporting, do this in a nicer way later */
error_reporting(-1);
ini_set('display_errors',1);
ini_set("log_errors", 1);


$application = (! is_dir($application) AND is_dir(ROOT.$application))   ? ROOT.$application : $application;
$system      = (! is_dir($system) AND is_dir(ROOT.$system))             ? ROOT.$system      : $system;
$extensions  = (! is_dir($extensions) AND is_dir(ROOT.$extensions))     ? ROOT.$extensions  : $extensions;
$storage  	 = ROOT.$storage;

define('APPATH', $application.DS);
define('SYSPATH', $system.DS);
define('EXTPATH', $extensions.DS);
define('STOREPATH', $storage.DS);
define('DEFAULT_CONTROLLER', 'Index');
define('DEFAULT_ACTION', 'Index');

unset($application, $system);

include APPATH.'bootstrap'.EXT;
