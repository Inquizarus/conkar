#!/usr/bin/env php
<?php
require __DIR__.'/index.php';
if( isCli() ) :
	$application = New Conkar;
    spl_autoload_unregister('autoloadClasses');
    spl_autoload_register(function ($class) use ($application) {
        call_user_func_array('autoloadClasses', [$class, $application]);
    });
	$odin = Console::run($application);
	$odin->bind($argv); // Bind params passed to odin for command to use
	$odin->execute();
	$application->deinit();
endif;
exit(0);