<?php
/**
 * Array that determines what extensions should be loaded and set as active
 * in the framework.
 * All extensions are organized under namespaces with an extensionname as key and a config
 * that determines if its active and other things.
 */
return array(
  /** 'namespace' => array(
          'extension_name' => array(
              'folder' => 'folder',
              'active' => false //true or false
          ),
      )**/
);
