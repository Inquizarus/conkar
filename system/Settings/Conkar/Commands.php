<?php
	return array(
		'generate' => 'Conkar_Console_Commands_Generate',
		'generate:controller' => 'Conkar_Console_Commands_GenerateController',
		'generate:model' => 'Conkar_Console_Commands_GenerateModel',
		'clear:cache' => 'Conkar_Console_Commands_ClearCache',
		'runtests' => 'Conkar_Console_Commands_RunTests',
		);