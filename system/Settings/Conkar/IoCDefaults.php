<?php
return array(
  'Model' => function ($name,$source = false,Conkar $application = null) {
    return Model::factory($name,$source,$application);
  },
  'View' => function ($file) {
    try{
      return View::factory($file);
    }
    catch(Exception $e){
      echo $e;
      exit(1);
    }
  },
  'loadConfig' => function ($name, array $extensions = [], Conkar $application = null) {
    try{
      if (empty($extensions) && $application) {
        $extensions = $application->loadedExtensions();
      }
      return Config::loadFile($name,$extensions);
    }
    catch(Exception $e){
      echo $e;
      exit(1);
    }
  },
  'loadSpecificConfig' => function ($name,$which = null) {
    try{
      return Config::load($name,$which);
    }
    catch(Exception $e){
      echo $e;
      exit(1);
    }
  },
  'setConfig' => function ($name, $settings) {
    try{
      return Config::set($name, $settings);
    }
    catch(Exception $e){
      echo $e;
      exit(1);
    }
  },
  'eventContainer' =>  function(){
    try{
      return Event::factory();
    }
    catch(Exception $e){
      echo $e;
      exit(1);
    }
  },
  'fileDriver' => function($path = null, $file = null){
    try{
      return new Conkar_Logger_Drivers_File($path,$file);
    }
    catch(Exception $e){
      echo $e;
      exit(1);
    }
  },
  'logger' => function($name, Conkar_Interfaces_Logger_DriverInterface $driver, $application = null){
    try{
      $logger = Logger::factory($name,$driver);
      $logger->application($application);
      return $logger;
    }
    catch(Exception $e){
      echo $e;
      exit(1);
    }
  },
  'Request' => function ($uri = false,Conkar $application = null) {
    try{
      $request = Request::factory($uri);
      if (is_null($request) === false) {
        $request->application($application);
      }
      return $request;
    }
    catch(Exception $e){
      echo $e;
      exit(1);
    }
  },
  'CCurl' => function ($base_url = null, $follow = true, $returnTransfer = true) {
    try{
      return CCurl::factory($base_url, $follow, $returnTransfer);
    }
    catch(Exception $e){
      echo $e;
      exit(1);
    }
  },
  'FileCache' => function($subfolder = '',$cachetime = false, $extension = false, $application = null){
    $subfolder = (empty($subfolder)) ? $subfolder : ltrim($subfolder,DS);
    $driver = Conkar_Cache_Drivers_File::factory('storage/cache'.$subfolder,$cachetime,$extension);
    $cache = Cache::factory($driver);
    $cache->application($application);
    return $cache;
  },
  'CookieCache' => function($cachename = '',$cachetime = false, $extension = false, $application = null){
    $driver = Conkar_Cache_Drivers_Cookie::factory($cachename,$cachetime);
    $cache = Cache::factory($driver);
    $cache->application($application);
    return $cache;
  },
);
