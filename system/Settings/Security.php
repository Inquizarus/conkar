<?php 
	return array(
		/**
		 * application_salt Will be used when hashing strings within the application.
		 * Use a random string, leaving it blank will lower the strength of any hashes
		 * generated.
		 */
		'application_salt' => '',
		/**
		 * Determines what encryption cipher will be used then application encrypts
		 * data.
		 */
		'application_encryption_method' => MCRYPT_BLOWFISH,
		/**
		 * The encryption key will be used when the application encrypts data,
		 * do not use the default one. Generate a random string with a generator.
		 * a Good one is http://randomkeygen.com/
		 * --------------- WARNING! -------------
		 * Changing this after encrypting data will result in the data to be
		 * un-encryptable without brute-forcing it.
		 */
		'application_encryption_key' => 'Od!n#w4s#th3#f4th3r#0f#n0n3'
	);