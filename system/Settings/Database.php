<?php
/**
 * Returns an array of configurations for instances of database connections.
 * The framework will use the one with key "default" when nothing else have been defined
 */

return array(
        'default' => array(
            'database'  => '',      // Name of the database you want to select and work with
            'hostname'  => '',      // Where do the database recide? Usally localhost.
            'username'  => '',      // Which user to login as when connecting to database
            'password'  => ''       // Password for the named database user
            ),
    );