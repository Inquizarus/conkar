<?php
/**
 * An array of eventlisteners that should be called when a specific event is called.
 * When an event is called and a listener runs it will recieve event data as a parameter.
 * Methods that are invoked are called like a static method. Non static methods will generate a warning
 */
return array(
  /**
   * Eventname determine what event the listeners concern.
   */
  'eventname' => array(
    /**
     * Each listener is an array
     */
    array(
      /**
       * Priority determines in which order the events will fire
       */
      'priority' => 1,
      /**
       * class and method determines more or less what will happend with the eventdata.
       */
      'class' => 'classname',
      'method' => 'methodname'
    ),
  ),
);