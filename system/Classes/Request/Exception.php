<?php
class Request_Exception extends Conkar_Exception 
{
    public static $error_view              = 'Error/404';
    public static $error_view_content_type = 'text/html';
}