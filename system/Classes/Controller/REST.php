<?php
class Controller_REST extends Controller {

	const CONKAR_API_SUCCESS = 1;
	const CONKAR_API_FAILURE = 2;

	protected $codeMessages = [
		self::CONKAR_API_SUCCESS => 'OK',
		self::CONKAR_API_FAILURE => 'FAILURE'
	];

	protected $data = [
			'meta' => [
				'code' => self::CONKAR_API_SUCCESS,
				'message' => '',
				'errors'  => []
			],
			'body' => []
		];

	protected $formats  = ['json','xml'];
	protected $methods  = ['get','post','put','delete'];
	protected $required = [];

	protected $format = 'json';
	protected $fpc	  = false;

      /**
     * Runs the action that have been determined in the requestobject earlier
     * @param string $action
     */
    public function execute($action){
      	$this->before();
      	$method = strtolower($this->request->method());
      	$methodAllowed = in_array($method, $this->methods);
	    if(method_exists($this, $action) && $methodAllowed === true){
	    	$missing = array_requires($this->request->input(),$this->required);

	    	if ( empty($missing)) {
	    		$this->$action();
	    	} else {
	    		$this->code(static::CONKAR_API_FAILURE);
		    	$this->message('Missing required parameters');
		    	foreach($missing as $key => $value){
		    		if(is_string($value)){
		    			$this->error('Invalid parameter type, got '.$value.' and expected '.$this->required[$key]);
		    		}
		    		else {
		    			$this->error('Parameter missing: '.$key);
		    		}
		    	}
	    	}
	    }else{
	    	if($methodAllowed === true){
	    		$this->code(static::CONKAR_API_FAILURE);
		    	$this->message('Requested endpoint is not implemented or not accessible');
		    	$this->error('Invalid endpoint');
	    	}else{
				$this->code(static::CONKAR_API_FAILURE);
		    	$this->message('Request Method not allowed, allowed methods ['.implode(', ', $this->methods).']');
		    	$this->error('Method '.strtoupper($method).' Not allowed');
	    	}
	    }
      	$this->after();
    }

	public function before(){
      $this->application()->event()->triggerEvent('conkar_controller_rest_before_start',array('eventObject' => $this));
		$this->response->fpc($this->fpc);
		$format = $this->request->input('format');

		if(!empty($format)){
			$this->format($format);
		}
      $this->application()->event()->triggerEvent('conkar_controller_rest_before_end',array('eventObject' => $this));
	}

	public function getIndex(){}
	public function postIndex(){}
	public function putIndex(){}
	public function deleteIndex(){}

	public function message($message = null){
		if ( $message ) {
      		$this->application()->event()->triggerEvent('conkar_controller_rest_set_message',array('eventData' => [&$message]));
			$this->data['meta']['message'] = (string) $message;
			return $this;
		}
		else{
			return $this->data['meta']['message'];
		}
	}


	/**
	 * @param integer $code
	 */
	public function code($code = null){
		if ( $code ) {
      		$this->application()->event()->triggerEvent('conkar_controller_rest_set_code',array('eventData' => [&$code]));
			$this->data['meta']['code'] = (string) $code;
			return $this;
		}
		else{
			return $this->data['meta']['code'];
		}
	}

	/**
	 * @param string $error
	 */
	public function error($error){
		$this->data['meta']['errors'][] = (string) $error;
		return $this;
	}

	public function errors(array $errors = array()){
		if ( ! empty($errors) ) {
      		$this->application()->event()->triggerEvent('conkar_controller_rest_set_errors',array('eventData' => [&$errors]));
			$this->data['meta']['errors'] = $errors;
			return $this;
		} else {
			return $this->data['meta']['errors'];
		}
	}

	public function clearErrors(){
		$this->data['meta']['errors'] = [];
		return $this;
	}

	public function format($format = null){
		if($format){
			$format = strtolower($format);
			if( in_array($format, $this->formats) ) {
				$this->format = $format;

				return $this;
			}else{
				$this->code(static::CONKAR_API_FAILURE);
				$message = 'Format '.$format.' not allowed, formats available ['.implode(', ', $this->formats).']';
				$this->error($message);
			}

		}else{
			return $this->format;
		}
	}

	public function body(array $content = null){
		if ( $content ) {
	      	$this->application()->event()->triggerEvent('conkar_controller_rest_set_body',array('eventData' => [&$content]));
			$this->data['body'] = $content;
			return $this;
		}
		else{
			return $this->data['body'];
		}
	}

    /**
    * Method that is called after determined action
     * Do not forget to call the parent method with parent::after()
     */
    public function after(){
      $this->application()->event()->triggerEvent('conkar_controller_rest_after_start',array('eventObject' => $this));

      if( empty($this->data['meta']['message']) ) {
      	$this->message($this->codeMessages[$this->code()]);
      }

      	switch ($this->format) {
      		case 'json':
      			$this->response->sendJson();
      			$this->response->jsonBody($this->data);
      			break;
      		case 'xml':
      			$this->response->sendXml();
      			$this->response->xmlBody($this->data);
      			break;
      	}
      	$this->application()->event()->triggerEvent('conkar_controller_rest_after_end',array('eventObject' => $this));
        $this->response->send();
    }

}
