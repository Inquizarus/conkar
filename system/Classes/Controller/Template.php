<?php
/**
 * Template class for controllers that are supposed to be used as regular frontend controllers.
 * Supports autorender, automerge and declaration of css and javascripts into template view.
 * Also holds a "ghost" index action that will output some short content.
 */
class Controller_Template extends Controller{

    /**
     * @var bool
     */
    public $_auto_merge     = False;
    /**
     * @var array
     */
    public $_css            = array();
    /**
     * @var array
     */
    public $css             = array();
    /**
     * @var array
     */
    public $_scripts        = array();
    /**
     * @var array
     */
    public $scripts         = array();
    /**
     * @var array
     */
    public $_bottomScripts  = array();
    /**
     * @var array
     */
    public $bottomScripts   = array();

    /**
     * Add a javascript file path to an array that enables
     * @param   $path
     * @param   $hidden
     * @return  $this
     */
    public function addBottomScript($path, $hidden = false){
      $this->event('conkar_controller_template_addBottomScript',['eventData' => [&$path,&$hidden]]);
        if($hidden){
            $this->_bottomScripts[] = $path;
        }
        else{
        $this->bottomScripts[] = $path;
        }
        return $this;
    }

    /**
     * @param   $path
     * @param   $hidden
     * @return  $this
     */
    public function addCss($path, $hidden = false){
        $this->event('conkar_controller_template_addCss',[&$path,&$hidden]);
        if($hidden){
            $this->_css[] = $path;
        }
        else{
        $this->css[] = $path;
        }
        return $this;
    }

    /**
     * @param   $path
     * @param   $hidden
     * @return  $this
     */
    public function addScript($path, $hidden = false){
      $this->event('conkar_controller_template_addScript',['eventData' => [&$path,&$hidden]]);
        if($hidden){
            $this->_scripts[] = $path;
        }
        else{
        $this->scripts[] = $path;
        }
        return $this;
    }


    public function after(){
      $this->event('conkar_controller_template_after_start',['eventObject' => $this]);
        if($this->_auto_merge && ! empty($this->template)){
            $this->template->css              = array_merge($this->_css, $this->css);
            $this->template->scripts          = array_merge($this->_scripts, $this->scripts);
            $this->template->bottomScripts    = array_merge($this->_bottomScripts, $this->bottomScripts);
        }
      parent::after();
      $this->event('conkar_controller_template_after_end',['eventObject' => $this]);
    }

    public function getIndex(){
      echo 'Running action '.$this->request->action().' in controller '. $this->request->controller();
    }

}
