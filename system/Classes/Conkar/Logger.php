<?php
/**
 * Class that enables logging to media determined by the driver passed into it during construction
 */
abstract class Conkar_Logger extends ConkarObject
{
    /**
     * Instance of the driver that is used to create logs
     * @var Conkar_Interfaces_Logger_DriverInterface
     */
    private $logDriver = null;
    /**
     * Determines what the minimum level of logging is
     * @var int
     */
    private $minLogLevel = Conkar_Logger_Level::DEBUG;
    /**
     * Determines what format DateTime stamps should have in entries
     * @var string
     */
    private $dateFormat = 'D M j H:i:s y';
    /**
     * Determines what permissionlevels should be set when creating new folders
     * and files
     * @var integer
     */
    private $defaultPermissions = 0777;
    /**
     * Container that holds all instances of loggers that have been created
     * through factory method
     * @var array
     */
    public static $_logger_container = array();

    /**
     * Constructor should not be called directly, use class factory method
     */
    public function __construct(Conkar_Interfaces_Logger_DriverInterface $driver)
    {
        $this->logDriver = $driver;
    }

    /**
     * Creates and returns an instance of Logger
     * @param  string                          $name
     * @param  Conkar_Interfaces_Logger_DriverInterface $driver
     * @return Logger
     */
    public static function factory($name, Conkar_Interfaces_Logger_DriverInterface $driver)
    {
        $logger = new Logger($driver);

        self::$_logger_container[$name] = $logger;
        return self::$_logger_container[$name];
    }

    /**
     * Retrieves a logger instance from static container if found.
     * @param  string $name
     * @return Logger
     */
    public static function get($name)
    {

        if (empty(self::$_logger_container[$name])) {
            new Exception;
        }

        return self::$_logger_container[$name];

    }

    /**
     * Set the dateformat to use when making entries
     * @param string $format
     */
    public function setDateFormat($format)
    {
        $this->dateFormat = $format;
        return $this;
    }

    /**
     * System is unusable.
     *
     * @param string $message
     * @param array $context
     */
    public function emergency($message, array $context = array())
    {
        $this->log(Conkar_Logger_Level::EMERGENCY, $message, $context);
    }

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string $message
     * @param array $context
     */
    public function alert($message, array $context = array())
    {
        $this->log(Conkar_Logger_Level::ALERT, $message, $context);
    }

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string $message
     * @param array $context
     */
    public function critical($message, array $context = array())
    {
        $this->log(Conkar_Logger_Level::CRITICAL, $message, $context);
    }

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $message
     * @param array $context
     */
    public function error($message, array $context = array())
    {
        $this->log(Conkar_Logger_Level::ERROR, $message, $context);
    }

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string $message
     * @param array $context
     */
    public function warning($message, array $context = array())
    {
        $this->log(Conkar_Logger_Level::WARNING, $message, $context);
    }

    /**
     * Normal but significant events.
     *
     * @param string $message
     * @param array $context
     */
    public function notice($message, array $context = array())
    {
        $this->log(Conkar_Logger_Level::NOTICE, $message, $context);
    }

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string $message
     * @param array $context
     */
    public function info($message, array $context = array())
    {
        $this->log(Conkar_Logger_Level::INFO, $message, $context);
    }

    /**
     * Detailed debug information.
     *
     * @param string $message
     * @param array $context
     */
    public function debug($message, array $context = array())
    {
        $this->log(Conkar_Logger_Level::DEBUG, $message, $context);
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     */
    public function log($level, $message, array $context = array())
    {
        $this->event()->triggerEvent('conkar_logger_log_start',['eventObject' => $this,'eventData' => [
                'level' => &$level,
                'message' => &$message,
                'context' => &$context
            ]]);

        $message = interpolate($message, $context);
        $this->logDriver->write($level, $message, date($this->dateFormat, time()));
    }

}
