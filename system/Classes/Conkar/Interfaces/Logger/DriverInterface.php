<?php
interface Conkar_Interfaces_Logger_DriverInterface {

  /**
   * Writes/Pushes entry to log
   * @param  string $entry [description]
   * @param string $datetime
   * @return void        [description]
   */
  public function write($level, $entry, $datetime);
}
