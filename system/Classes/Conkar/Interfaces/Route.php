<?php
interface Conkar_Interfaces_Route {
    /**
     * @return void
     */
    public function __construct($data);

    /**
     * @param  string    $name
     * @param  string    $uri
     * @return mixed
     */
    public static function set($name,$uri);

    /**
     * @param  string    $name
     * @return mixed
     */
    public static function get($name);

    /**
     * @param  string $uri
     * @param  array  $routes
     * @return mixed
     */
    public static function match($uri,$routes);
}
