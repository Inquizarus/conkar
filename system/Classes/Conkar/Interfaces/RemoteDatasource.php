<?php
interface Conkar_Interfaces_RemoteDatasource{

  /**
   * Opens up or retrieves connection to the datasource. For example a database or an API
   */
  public function connect();


  /**
   * Closes/Destroys the connection the datasource
   */
  public function disconnect();

  /**
   * Add restrictions to what to retrieve 
   * 
   * @return object
   */
  public function select($select = '');

  public function fetch();

  public function fetchAll();

}
