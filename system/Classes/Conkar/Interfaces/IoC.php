<?php
interface Conkar_Interfaces_IoC
{
    /**
     * @param  string      $name
     * @param  Closure    $lambda
     * @return mixed
     */
    public function register($name, Closure $lambda);

    /**
     * @param string   $key
     * @param array    $params
     * @return mixed
     */
    public function make($key, $params);

    /**
     * @param string   $key
     * @return boolean
     */
    public function registered($key);
}
