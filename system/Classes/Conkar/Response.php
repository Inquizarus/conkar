<?php
abstract class Conkar_Response extends ConkarObject {

    const HTTP_CONTINUE                                                  = 100;
    const HTTP_SWITCHING_PROTOCOLS                                       = 101;
    const HTTP_PROCESSING                                                = 102; // RFC2518
    const HTTP_OK                                                        = 200;
    const HTTP_CREATED                                                   = 201;
    const HTTP_ACCEPTED                                                  = 202;
    const HTTP_NON_AUTHORITATIVE_INFORMATION                             = 203;
    const HTTP_NO_BODY                                                   = 204;
    const HTTP_RESET_BODY                                                = 205;
    const HTTP_PARTIAL_BODY                                              = 206;
    const HTTP_MULTI_STATUS                                              = 207; // RFC4918
    const HTTP_ALREADY_REPORTED                                          = 208; // RFC5842
    const HTTP_IM_USED                                                   = 226; // RFC3229
    const HTTP_MULTIPLE_CHOICES                                          = 300;
    const HTTP_MOVED_PERMANENTLY                                         = 301;
    const HTTP_FOUND                                                     = 302;
    const HTTP_SEE_OTHER                                                 = 303;
    const HTTP_NOT_MODIFIED                                              = 304;
    const HTTP_USE_PROXY                                                 = 305;
    const HTTP_RESERVED                                                  = 306;
    const HTTP_TEMPORARY_REDIRECT                                        = 307;
    const HTTP_PERMANENTLY_REDIRECT                                      = 308; // RFC7238
    const HTTP_BAD_REQUEST                                               = 400;
    const HTTP_UNAUTHORIZED                                              = 401;
    const HTTP_PAYMENT_REQUIRED                                          = 402;
    const HTTP_FORBIDDEN                                                 = 403;
    const HTTP_NOT_FOUND                                                 = 404;
    const HTTP_METHOD_NOT_ALLOWED                                        = 405;
    const HTTP_NOT_ACCEPTABLE                                            = 406;
    const HTTP_PROXY_AUTHENTICATION_REQUIRED                             = 407;
    const HTTP_REQUEST_TIMEOUT                                           = 408;
    const HTTP_CONFLICT                                                  = 409;
    const HTTP_GONE                                                      = 410;
    const HTTP_LENGTH_REQUIRED                                           = 411;
    const HTTP_PRECONDITION_FAILED                                       = 412;
    const HTTP_REQUEST_ENTITY_TOO_LARGE                                  = 413;
    const HTTP_REQUEST_URI_TOO_LONG                                      = 414;
    const HTTP_UNSUPPORTED_MEDIA_TYPE                                    = 415;
    const HTTP_REQUESTED_RANGE_NOT_SATISFIABLE                           = 416;
    const HTTP_EXPECTATION_FAILED                                        = 417;
    const HTTP_I_AM_A_TEAPOT                                             = 418;                                               // RFC2324
    const HTTP_UNPROCESSABLE_ENTITY                                      = 422;                                        // RFC4918
    const HTTP_LOCKED                                                    = 423;                                                      // RFC4918
    const HTTP_FAILED_DEPENDENCY                                         = 424;                                           // RFC4918
    const HTTP_RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL = 425; // RFC2817
    const HTTP_UPGRADE_REQUIRED                                          = 426;                                            // RFC2817
    const HTTP_PRECONDITION_REQUIRED                                     = 428;                                       // RFC6585
    const HTTP_TOO_MANY_REQUESTS                                         = 429;                                           // RFC6585
    const HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE                           = 431; // RFC6585
    const HTTP_INTERNAL_SERVER_ERROR                                     = 500;
    const HTTP_NOT_IMPLEMENTED                                           = 501;
    const HTTP_BAD_GATEWAY                                               = 502;
    const HTTP_SERVICE_UNAVAILABLE                                       = 503;
    const HTTP_GATEWAY_TIMEOUT                                           = 504;
    const HTTP_VERSION_NOT_SUPPORTED                                     = 505;
    const HTTP_VARIANT_ALSO_NEGOTIATES_EXPERIMENTAL                      = 506; // RFC2295
    const HTTP_INSUFFICIENT_STORAGE                                      = 507;                                        // RFC4918
    const HTTP_LOOP_DETECTED                                             = 508;                                               // RFC5842
    const HTTP_NOT_EXTENDED                                              = 510;                                                // RFC2774
    const HTTP_NETWORK_AUTHENTICATION_REQUIRED                           = 511; // RFC6585

    /**
     * Status codes translation table.
     *
     * The list of codes is complete according to the
     * {@link http://www.iana.org/assignments/http-status-codes/ Hypertext Transfer Protocol (HTTP) Status Code Registry}
     * (last updated 2012-02-13).
     *
     * Unless otherwise noted, the status code is defined in RFC2616.
     *
     * @var array
     */
    public static $statusTexts = array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',            // RFC2518
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Body',
        205 => 'Reset Body',
        206 => 'Partial Body',
        207 => 'Multi-Status',          // RFC4918
        208 => 'Already Reported',      // RFC5842
        226 => 'IM Used',               // RFC3229
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => 'Reserved',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',    // RFC7238
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',                                               // RFC2324
        422 => 'Unprocessable Entity',                                        // RFC4918
        423 => 'Locked',                                                      // RFC4918
        424 => 'Failed Dependency',                                           // RFC4918
        425 => 'Reserved for WebDAV advanced collections expired proposal',   // RFC2817
        426 => 'Upgrade Required',                                            // RFC2817
        428 => 'Precondition Required',                                       // RFC6585
        429 => 'Too Many Requests',                                           // RFC6585
        431 => 'Request Header Fields Too Large',                             // RFC6585
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates (Experimental)',                      // RFC2295
        507 => 'Insufficient Storage',                                        // RFC4918
        508 => 'Loop Detected',                                               // RFC5842
        510 => 'Not Extended',                                                // RFC2774
        511 => 'Network Authentication Required',                             // RFC6585
    );

    /** @var string[] $headers HTTP Headers that will be sent */
    public    $headers  = array();

    /** @var int $statusCode Current response HTTP Status Code */
    protected $statusCode;

    /** @var string $body Content that will be sent */
    protected $body;

    /** @var bool $fpc Flag that determines if fullpage cache should be used */
    protected $fpc = false;

    /**
     * @param string $body
     * @param integer $status
     */
    public function __construct($body, $status, $headers){
        $this->setBody($body);
        $this->setStatusCode($status);
        $this->setHeaders($headers);
    }

    public static function factory($body = '', $status = 200, $headers = []){
        return new Response($body, $status, $headers);
    }

    /**
     * Doubles as getter and setter for body
     * @param  mixed $body
     * @return Response|String
     */
    public function body($body = null){
        if($body) {
         return $this->setBody($body);
        }
        else{
            return $this->getBody();
        }
    }


    public function setRedirect($url, $code = Response::HTTP_FOUND){
        $url = ltrim($url,'/');
        $this->setHeader('Location',baseUrl().$url);
        $this->setStatusCode($code);
        return $this;
    }

    public function setUrlRedirect($url, $code = Response::HTTP_FOUND){
        $this->setHeader('Location',$url);
        $this->setStatusCode($code);
        return $this;
    }


    /**
     * Get the current body of this response
     * @return string
     */
    public function getBody(){
        return $this->body;
    }


    /**
     * Get the current headers set
     * @return array
     */
    public function getHeaders(){
        return $this->headers;
    }

    /**
     * @param boolean $mode
    */
    public function fpc($mode = null){
        if( ! is_null($mode) ){
            $this->fpc = (bool) $mode;
            return $this;
        }else{
            return $this->fpc;
        }
    }


    /**
     * Doubles as a getter and setter for response headers
     * @param  array|null $headers
     * @return Reponse|Array
     */
    public function headers($headers = null){

        if($headers AND is_array($headers)){
            return $this->setHeaders($headers);
        }
        else{
            return $this->getHeaders();
        }

    }


    /**
     * Outputs both headers and content
     * @return string
     */
    public function send()
    {

        $this->application()->event()->triggerEvent('response_send_start',['eventObject' => $this]);

        if ($this->isNotFound()) {
            $view = View::factory('Error/404');
            $this->body($view->render());
        }

        if ($this->isServerError()) {
            $view = View::factory('Error/500');
            $this->body($view->render());
        }

        $this->startGenerateBody();

        $this->sendHeaders();

        if( ! empty($this->body) AND ! $this->isRedirection()) {
             $this->sendBody();
        }

        $data = $this->endGenerateBody();

        $this->body($data);
        $this->sendBody();
    }

    public function startGenerateBody()
    {
        if($this->cache()) {
            ob_start();
        }
    }

    public function endGenerateBody()
    {
        $data = ob_get_contents();
        ob_end_clean();

        $this->application()->event()->triggerEvent('response_data_generated',['eventData' => &$data]);

        if($this->cache()) {
            if( ! $this->isRedirection() AND $this->fpc() == true) {
                $this->cache()->store($this->application()->request()->uri(),$data);
            }
        }
        return $data;
    }

    /**
     * Sends the headers set
     * @return Response
     */
    public function sendHeaders()
    {
        $this->application()->event()->triggerEvent('response_headers_send_before',['eventObject' => $this]);
        foreach ($this->headers as $headerName => $headerValue) {
            header($headerName . ': ' . $headerValue);
        }
        $status = $this->status();
        $text   = (string) $status.' '.static::$statusTexts[$status];
        header("HTTP/1.1 $text", true, $status);
        $this->application()->event()->triggerEvent('response_headers_send_after',['eventObject' => $this]);
        return $this;
    }

    /**
     * Sets the body to be sent as output
     * @param  mixed $body anything that can be treated as a string
     * @return Response
     */
    public function setBody($body){
        if (null !== $body && !is_string($body) && !is_numeric($body) && !is_callable(array($body, '__toString'))) {
            throw new Exception(sprintf('The Response body must be a string or object implementing __toString(), "%s" given.', gettype($body)));
        }
        $this->body = (string) $body;
        return $this;
    }


    /**
     * Outputs the content set
     * @return string
     */
    public function sendBody(){

        if($this->isXmlResponse()){
            $view = new View();
            $view->application($this->application());
            $view->file('Xml');
            $view->xml = $this->body();
            $this->body(trim($view->render(),' '));
        }
        echo $this->body();
    }

    /**
     * Adds more content to the existing content
     * @param  mixed $content anything that can be treated as a string
     * @return Response
     */
    public function concatBody($content){
        $body = $this->body();
        $this->body($content)->body($body.$this->body());
        return $this;
    }

    /**
     * Sets a specific header to a specific value
     * @param string $name
     * @param string $value
     * @return Response
     */
    public function setHeader($name, $value){

        $this->headers[$name] = $value;

        return $this;
    }

    /**
     * Sets multiple headers
     * @param array $headers associative array
     * @return Response
     */
    public function setHeaders(array $headers){

        foreach ($headers as $name => $value) {
            if(is_string($name)){
                $this->setHeader($name, $value);
            }
        }

        return $this;
    }

    /**
     * Empties all  headers set
     * @return Response
     */
    public function clearHeaders(){
        $this->headers = [];
        return $this;
    }

    /**
     * Sets the HTTP Response Code
     * @param int $status
     */
    public function setStatusCode($status){
            $this->statusCode = (int) $status;
        return $this;
    }

    /**
     * Gets the current HTTP Status Code
     * @return int Current HTTP Status Code
     */
    public function status(){
        return $this->statusCode;
    }

    public function sendJson(){
        $this->setHeader('Content-Type','application/json');
        return $this;
    }

    public function jsonBody($body = null){
        $body = ($body) ? $body : $this->body();
        @json_decode($body);
        if( json_last_error() == JSON_ERROR_NONE ){
            $this->body(json_encode($body));
        }
        return $this;
    }


    public function xmlBody($body = null){
        $body = ($body) ? $body : $this->body();
        if(is_array($body)){
            $this->body(assoc_array_xml($body));
        }
        return $this;
    }

    public function sendXml(){
        $this->setHeader('Content-Type','text/xml');
        return $this;
    }

    public function isXmlResponse(){
        return ( !empty($this->headers['Content-Type']) AND $this->headers['Content-Type'] == 'text/xml' );
    }

    public function sendHtml(){
        $this->setHeader('Content-Type','text/html');
        return $this;
    }

    /**
     * Is response invalid?
     *
     * @return bool
     *
     * @api
     */
    public function isInvalid()
    {
        return $this->statusCode < 100 || $this->statusCode >= 600;
    }
    /**
     * Is response informative?
     *
     * @return bool
     *
     * @api
     */
    public function isInformational()
    {
        return $this->statusCode >= 100 && $this->statusCode < 200;
    }
    /**
     * Is response successful?
     *
     * @return bool
     *
     * @api
     */
    public function isSuccessful()
    {
        return $this->statusCode >= 200 && $this->statusCode < 300;
    }
    /**
     * Is the response a redirect?
     *
     * @return bool
     *
     * @api
     */
    public function isRedirection()
    {
        return $this->statusCode >= 300 && $this->statusCode < 400;
    }
    /**
     * Is there a client error?
     *
     * @return bool
     *
     * @api
     */
    public function isClientError()
    {
        return $this->statusCode >= 400 && $this->statusCode < 500;
    }
    /**
     * Was there a server side error?
     *
     * @return bool
     *
     * @api
     */
    public function isServerError()
    {
        return $this->statusCode >= 500 && $this->statusCode < 600;
    }
    /**
     * Is the response OK?
     *
     * @return bool
     *
     * @api
     */
    public function isOk()
    {
        return 200 === $this->statusCode;
    }
    /**
     * Is the response forbidden?
     *
     * @return bool
     *
     * @api
     */
    public function isForbidden()
    {
        return 403 === $this->statusCode;
    }
    /**
     * Is the response a not found error?
     *
     * @return bool
     *
     * @api
     */
    public function isNotFound()
    {
        return 404 === $this->statusCode;
    }
    /**
     * Is the response a redirect of some form?
     *
     * @param string $location
     *
     * @return bool
     *
     * @api
     */
    public function isRedirect($location = null)
    {
        return in_array($this->statusCode, array(201, 301, 302, 303, 307, 308)) && (null === $location ?: $location == array_get($this->headers,'Location'));
    }
    /**
     * Is the response empty?
     *
     * @return bool
     *
     * @api
     */
    public function isEmpty()
    {
        return in_array($this->statusCode, array(204, 304));
    }



}
