<?php
class Conkar_Cache_Wrapper
{
    private $timeToLive = null;
    private $storedAt = null;
    private $data = null;

    public function data($data = null)
    {
        if ($data) {
            $this->data = $data;
            return $this;
        }

        return $this->data;
    }

    public function ttl($ttl = null)
    {
        if ($ttl) {
            $this->timeToLive = $ttl;
            return $this;
        }

        return $this->timeToLive;
    }

    public function stored($stored = null)
    {
        if ($stored) {
            $this->storedAt = $stored;
            return $this;
        }

        return $this->storedAt;
    }

    public function isExpired()
    {
        if ($this->ttl() < 1 || ($this->ttl() + $this->stored()) < time()) {
            return false;
        }
        return true;
    }



}
