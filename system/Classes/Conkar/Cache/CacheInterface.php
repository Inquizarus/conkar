<?php
interface Conkar_Cache_CacheInterface {
	public static function factory(Conkar_Cache_Drivers_CacheDriverInterface $driver);
	public function driver();
}