<?php
class Conkar_Cache_Drivers_Cookie extends Conkar_Cache_Drivers_AbstractDriver implements Conkar_Cache_Drivers_CacheDriverInterface
{

    private $cookieName,
        $cacheLifeTime;

    /**
     * @param string $cookieName
     * @param integer $cacheLifeTime
     */
    public function __construct($cookieName, $cacheLifeTime)
    {
        $this->cookieName    = $cookieName;
        $this->cacheLifeTime = $cacheLifeTime;
    }

    /**
     * Creates a new instance of drivers
     * @param  string  $cookieName    cookie to store data in
     * @param  integer $cachetime How long until data expires
     * @return Conkar_Cache_Drivers_CacheDriverInterface
     */
    public static function factory($cookieName = 'conkarcache', $cachetime = 18000)
    {
        return new Conkar_Cache_Drivers_Cookie($cookieName, $cachetime);
    }

    /**
     * @return string
     */
    public function cookieName()
    {
        return $this->cookieName;
    }

    public function cacheLifeTime()
    {
        return $this->cacheLifeTime;
    }

    /**
     * @param string $data
     */
    public function cookie($data = null)
    {
        if (!empty($data)) {
            try {
                setcookie($this->cookieName(), $data);
            } catch (Exception $e) {
                Conkar::log()->error($e->getMessage());
            }
            return $this;
        } else {
            return (isset($_COOKIE[$this->cookieName()])) ? $_COOKIE[$this->cookieName()] : null;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function store($name, $data)
    {
        $store = [
            'cached'  => time(),
            'expires' => time() + $this->cacheLifeTime(),
            'value'   => $data,
        ];

        $current        = $this->load();
        $current[$name] = $store;
        $current        = $this->encode($current);
        $this->cookie($current);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function load($name = false)
    {

        if ($data = $this->cookie()) {
            $data = $this->decode($data);
            if ($name) {
                if (isset($data[$name])) {
                    if (time() < (int) $data[$name]['expires']) {
                        return $data[$name]['value'];
                    }
                }
            } else {
                return $data;
            }
        }
        return null;
    }

    public function delete($name)
    {}

}
