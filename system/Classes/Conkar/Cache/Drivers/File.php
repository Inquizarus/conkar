<?php
class Conkar_Cache_Drivers_File extends Conkar_Cache_Drivers_AbstractDriver implements Conkar_Cache_Drivers_CacheDriverInterface {

	private $folder,
			$directory,
			$cachetime,
			$extension;


	public function __construct($folder, $cachetime, $extension){
		$this->folder($folder);
		$this->cacheTime($cachetime);
		$this->extension($extension);
 		prepareDirectories($this->folder());
		$path = ROOT.$this->folder();
		if(is_dir($path)){
			$this->directory = $path;
		}else{
			throw new Conkar_Exception('Could not find or create folder '.$path);
		}
	}


	/**
	 * Creates a new instance of drivers
	 * @param  string  $folder    Folder to store data in
	 * @param  integer $cachetime How long until data expires
	 * @param  string  $extension File extensions
	 * @return Conkar_Cache_Drivers_CacheDriverInterface
	 */
	public static function factory($folder = 'storage/cache', $cachetime = 18000, $extension = '.html'){
		return new Conkar_Cache_Drivers_File($folder,$cachetime,$extension);
	}


	/**
	 * {@inheritDoc}
	 */
	public function store($name, $data){
		$name = $this->generateName($name);
		$folder   = $this->generateFolderName($name);
		$path = $this->folder().DS.$folder;
		prepareDirectories($path);

		if(is_dir($path)){
			$name = $name.$this->extension();
			$entry = new CacheWrapper;
			$entry->data($data);
			$entry->ttl($this->getCacheTime());
			$entry->stored(time());
			file_put_contents($path.DS.$name, $this->encode($entry));
		}
		else{
			throw new Conkar_Exception('Cache folder dont exist and could not be created');
		}
	}


	/**
	 * {@inheritDoc}
	 */
	public function load($name){
		$name   	= $this->generateName($name);
		$folder     = $this->generateFolderName($name);
		$path 		= $this->directory.DS.$folder.DS.$name.$this->extension();

		if(file_exists($path)){
			$entry = $this->decode(file_get_contents($path));

			if ($entry->isExpired() === false) {
				return $entry->data();
			}

		}

		return null;
	}


	public function getDirectory(){
		return $this->directory;
	}

	public function getFolder(){
		return $this->folder;
	}

	public function cacheTime($time = null){
		return ($time) ? $this->setCacheTime($time) : $this->getCacheTime();
	}

	public function setCacheTime($time){
		$this->cachetime = (int) $time;
		return $this;
	}

	public function getCacheTime(){
		return $this->cachetime;
	}

	public function extension($extension = null){
		return ($extension) ? $this->setExtension($extension) : $this->getExtension();
	}

	public function setExtension($extension){
		$this->extension = $extension;
		return $this;
	}

	public function getExtension(){
		return $this->extension;
	}

	public function setFolder($folder){
		$this->folder = (string) $folder;
		return $this;
	}

	public function delete($name){
		$filename   = $this->generateName($name);
		$folder     = $this->generateFolderName($filename);
		$path 		= $this->directory.DS.$folder.DS.$filename.$this->extension();

		if(file_exists($path)){
			if(unlink($path)){
				return $this;
			}
		}

		return false;
	}


	public function exists($name){
		$filename = $this->generateName($name);
		$folder   = $this->generateFolderName($filename);

		return is_file($this->directory.DS.$folder.DS.$filename.$this->extension());

	}

	public function folder($folder = null){
		return ($folder) ? $this->setFolder($folder) : $this->getFolder();
	}

	public function generateName($string){
		$filename = sha1($string);
		return $filename;
	}

	public function generateFolderName($string){
		$folder   = substr($string, 0,4);
		return $folder;
	}


}
