<?php
abstract class Conkar_Cache_Drivers_AbstractDriver implements Conkar_Cache_Drivers_CacheDriverInterface {

	/**
	 * {@inheritDoc}
	 */
	public function encode($data){
		return serialize($data);
	}
	

	/**
	 * {@inheritDoc}
	 */
	public function decode($data){
		return unserialize($data);
	}

}