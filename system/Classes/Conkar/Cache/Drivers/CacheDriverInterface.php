<?php
interface Conkar_Cache_Drivers_CacheDriverInterface {

	/**
	 * Stores a set of data under a certain name
	 * @param  string $name name|cache-key to store data under
	 * @param  mixed $data  The data to be stored
	 * @return Conkar_Cache_Drivers_CacheDriverInterface
	 */
	public function store($name, $data);


	/**
	 * Loads a set of data under a certain name if existing
	 * @param  string $name name|cache-key to stored data
	 * @return mixed
	 */
	public function load($name);


	/**
	 * Encodes passed data to making it storable
	 * @param  mixed $data Data to encode
	 * @return mixed
	 */
	public function encode($data);


	/**
	 * Decode passed data to be readable by application again
	 * @param  mixed $data Data to encode
	 * @return mixed
	 */
	public function decode($data);

	/**
	 * Deletes the cache with passed name
	 * @param  string $name Name|Cache-Key of stored data
	 * @return Conkar_Cache_Drivers_CacheDriverInterface|Boolean
	 */
	public function delete($name);

}