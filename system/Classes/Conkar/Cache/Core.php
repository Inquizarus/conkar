<?php
class Conkar_Cache_Core extends ConkarObject implements Conkar_Cache_CacheInterface {

	private $driver;

	private $folder,
			$directory,
			$cachetime,
			$extension;

	public function __construct(Conkar_Cache_Drivers_CacheDriverInterface $driver){
		$this->driver = $driver;
	}

	public static function factory(Conkar_Cache_Drivers_CacheDriverInterface $driver){
		return new Cache($driver);
	}


	public function driver(){
		return $this->driver;
	}


	public function store($name, $content){
		$this->event('conkar_cache_store',['eventData' => ['name' => &$name, 'content' => &$content]]);
		return	$this->driver()->store($name, $content);
	}

	public function fetch($name){
		return	$this->load($name);
	}

	public function load($name){
		$this->event('conkar_cache_load_before',['eventData' => ['name' => &$name]]);
		$content = $this->driver()->load($name);
		$this->event('conkar_cache_load_after',['eventData' => ['name' => &$name, 'content' => &$content]]);
		return $content;
	}



}
