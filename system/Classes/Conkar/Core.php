<?php
/**
 * Low level core class of CONKAR-MVC
 */
abstract class Conkar_Core
{
    const CONKAR_FRAMEWORK_NAME = 'Conkar';

    const CONKAR_VERSION = '0.9.0';

    const DEVELOPMENT = 40;

    const PRODUCTION = 10;

    const STAGING = 20;

    const TESTING = 30;

    /** @var integer $envMode Determines what mode the framework is running in */
    public static $envMode = self::DEVELOPMENT;

    /** @var IoC $IoC Inversion of control container */
    protected $IoC = null;

    /** @var array $autoloaded_classes Contains names of all autoloaded classes */
    protected $autoloadedClasses = array();

    /** @var array $autoloaded_interfaces Contains names of all autoloaded Interfaces */
    protected $autoloadedInterfaces = array();

    /** @var CacheDriver $cache Framework system cache */
    protected $cache = null;

    /** @var Config $config Application config object */
    protected $config = null;

    /** @var boolean $errors Defines if the framework should handle errors and exceptions */
    protected $errors = false;

    /** @var Event $event Framework event handler */
    protected $event = null;

    /** @var boolean $_init Determines if application have been initialized */
    protected $init = false;

    /** @var array $loadedExtensions Contains information about loaded extensions */
    protected $loadedExtensions = array();

    /** @var Logger $log Framework system logger */
    protected $log = null;

    /** @var boolean $profiling Determines if profiling is active */
    protected $profiling = null;

    /** @var Request $request The current request */
    protected $request = null;

    /**
     * Create a new instance of Conkar/Core
     *
     * @param Config $config Application configuration
     */
    public function __construct(Config $config = null)
    {
        if (empty($config)) {
            $config = Config::load('Application');
        }
        $this->setConfig($config);
    }

    /**
     * Helper to interact with Core IoC
     * @return Conkar_IoC
     */
    public function IoC()
    {
        if (empty($this->IoC)) {
            $lambdas   = Config::loadFile(DS . 'Conkar' . DS . 'IoCDefaults');
            $this->IoC = new IoC($lambdas);
        }
        return $this->IoC;
    }

    public function cache()
    {
        if ($this->cache && false === is_object($this->cache)) {
            $cache = $this->make('FileCache',['',false,false]);
            $cache->application($this);
            $this->cache = $cache;
        }

        return $this->cache;
    }

    public function deinit()
    {
        self::event()->triggerEvent('conkar_deinit');
        if ($this->profiling) {
            Profiler::render();
        }
        exit();
    }

    /**
     * PHP error handler, converts all errors into ErrorExceptions. This handler
     * respects error_reporting settings.
     *
     * @throws  ErrorException
     * @return  boolean
     */
    public function error_handler($code, $error, $file = null, $line = null)
    {
        if (error_reporting() & $code) {
            throw new ErrorException($error, $code, 0, $file, $line);
        }
        return true;
    }

    /**
     * Helper to interact with Core Eventhandler
     */
    public function event()
    {
        if (empty($this->event)) {
            $this->event = $this->IoC()->make('eventContainer');
            return $this->event;
        }
        return $this->event;
    }

    /**
     * Get the current application config
     *
     * @return Config
     */
    public function getConfig()
    {
        return $this->config;
    }


    /**
     * Shortcut to register a instance in the IoC container
     * @param  string $name    Resolver name
     * @param  mixed $instance Something to store
     * @return mixed
     */
    public function instance($name, $instance = null)
    {
        return $this->IoC()->instance($name, $instance);
    }

    /**
     * Returns currently loaded extensions
     * @return array
     */
    public function loadedExtensions()
    {
        return $this->loadedExtensions;
    }

    /**
     * Creates system logger
     */
    public function log()
    {

        if (!empty($this->log)) {
            return $this->log;
        }

        $driver = $this->IoC()->make('fileDriver', array('log/system', 'core.log'));
        $logger = $this->IoC()->make('logger', array('system', $driver));
        $logger->application($this);
        $this->log = $logger;
    }

    /**
     * Shortcut to access IoC container and create and instance of
     * passed name and parameters.
     * @param  string $name   Resolver name
     * @param  false[]  $params Resolver parameters
     * @return mixed          Instance of desired resolver
     */
    public function make($name, $params = array())
    {
        return $this->IoC()->make($name, $params);
    }

    public function profiling()
    {
        return $this->profiling;
    }

    /**
     * Shortcut to register a Resolver in the IoC container
     * @param  string $name    Resolver name
     * @param  closure $lambda Resolver function
     * @return bool
     */
    public function register($name, $lambda)
    {
        return $this->IoC()->register($name, $lambda);
    }

    public function request()
    {
        return $this->request;
    }

    /**
     * Load routes into the system, either by passed array
     * or loaded from config.
     * @param  array $routes
     */
    public function routes($routes = null)
    {
        $routes = ($routes) ? $routes : $this->IoC()->make('loadConfig', array('Routes',$this->loadedExtensions(),$this) );
        foreach ($routes as $name => $route) {
            Route::set($name, $route);
        }
    }

    public function run()
    {
        if ($this->init) {
            return;
        }

        $this->init = true;
        $this->initErrorHandling();
        $this->initCacheHandling();
        $this->IoC();
        $this->event();
        $this->loadExtensions();
        $this->log();
        $this->event()->triggerEvent('conkar_init_start', ['eventObject' => $this]);
        $this->routes();
        $this->cache();

        if (isCli() === false) {

            if (isDirEmpty(APPATH . 'Classes' . DS . 'Controller')) {
                require SYSPATH . 'init' . EXT;
                exit;
            }

            $this->request = $this->make('Request',[false]);
            $this->request()->application($this);
            $this->request()->setEventHandler($this->event());
            $this->request()->setCacheHandler($this->cache());
            $this->request()->profiling($this->profiling());
            $this->request()->execute();
            $this->event()->triggerEvent('conkar_before_deinit');
            $this->deinit();
        }

    }

    /**
     * Sets a config that will determine how the application
     * will behave.
     *
     * @param Config $config Application configuration
     *
     * @return Conkar
     */
    public function setConfig(Config $config)
    {
        $this->config = $config;
        return $this;
    }

    /**
     * Shortcut to register a Singleton Resolver in the IoC container
     * @param  string $name    Singleton Resolver name
     * @param  closure $lambda Resolver function
     * @return boolean|null
     */
    public function singleton($name, $lambda)
    {
        return $this->IoC()->singleton($name, $lambda);
    }

    /**
     * Returns a string presenting the current framework version
     * @return string
     */
    public static function version()
    {
        return self::CONKAR_FRAMEWORK_NAME . ' version ' . self::CONKAR_VERSION;
    }



    private function initCacheHandling()
    {
        if ($this->getConfig()->get('cache')) {
            $this->cache = true;
        }
    }

    private function initErrorHandling()
    {
        if ($this->getConfig()->get('errors') === true) {
            $this->errors = true;
            set_exception_handler(array('Conkar_Exception', 'handler'));
            set_error_handler(array($this, 'error_handler'));
        }
    }

    public function initProfiling()
    {
        if ($this->getConfig()->get('profiling')) {
            $this->profiling = true;
            Profiler::start('framework');
        }
    }

    /**
     * Loads extensions determined by settings file.
     */
    private function loadExtensions()
    {
        $config = $this->IoC()->make('loadConfig', array('Extensions', $this->loadedExtensions(), $this));
        foreach ($config as $namespace => $extensions) {
            foreach ($extensions as $name => $extensionEntry) {
                if (!empty($extensionEntry['active'])) {
                    $this->loadedExtensions[$namespace][$name] = $extensionEntry;
                    $folder = (empty($extensionEntry['folder'])) ? '' : $extensionEntry['folder'] . DS ;
                    $extensionObject = require EXTPATH . $namespace . DS . $folder . 'init.php';
                    $extensionObject->initialize($this);
                    $this->loadedExtensions[$namespace][$name]['object'] = $extensionObject;
                }
            }
        }
    }


}
