<?php
/**
 * Class that manages all config loading from files
 */
abstract class Conkar_Config extends ConkarObject implements Conkar_Interfaces_Config
{
    /**
     * Currently loaded configuration files
     * @var array
     */
    public static $_configurations = array();


    public function __construct(Array $config){
        foreach($config AS $key => $value){
            $this->set($key,$value);
        }
    }

    /**
     * Returns a specific configuration setting if it has been set
     * @param  mixed $key
     * @return mixed
     */
    public function get($key){
        if( ! empty($this->{$key}) ){
            return $this->{$key};
        }
        return false;
    }


    /**
     * Sets a config
     * @param mixed $key
     * @param mixed $value
     */
    public function set($key,$value){

        if(is_array($value)){
                $this->{$key} = new Config($value);
            }
        else{
                $this->{$key} = $value;
            }
    }


    /**
     * Loads a configuration file by filename. If file is not found in application scope then
     * the system scope will be checked
     * @param string $file
     */
    public static function loadFile($file, array $extensions = []){
        $path = APPATH.'Settings'.DS.$file.EXT;
        
        if( ! file_exists($path)){
            $found = false;
           foreach ($extensions AS $namespace => $extensions) {
              foreach($extensions AS $name => $extension){
                $path = EXTPATH.$namespace.DS.$extension['folder'].DS.'Settings'.DS.$file.EXT;
                if(file_exists($path)){
                  $found = true;
                  break;
                }
              }
              if($found) break;
            }
        }


        if( ! file_exists($path)){
            $path = SYSPATH.'Settings'.DS.$file.EXT;
        }

        if( file_exists($path) ){
            return  require $path;
        }

        return [];
    }


    /**
     * Registers a new configration and returns it if successfull
     * @param  string $key
     * @param  array  $settings
     * @return mixed
     */
    public static function register($key,Array $settings = null){

        if( ! array_key_exists($key, self::$_configurations)){
            if( ! empty($settings)){
                self::$_configurations[$key] = new Config($settings);
            }
            else{
                self::$_configurations[$key] = new Config(self::loadFile($key));
            }
            return self::load($key);
        }
        else{
            return false;
        }
    }


    /**
     * Loads a settings file in the form of a  configration object or a specified configuration
     * setting if subkey is passed
     * @param string $subkey
     */
    public static function load($key,$subkey = null){

        if(array_key_exists($key, self::$_configurations)){
           if( ! empty($subkey)){
                if(array_key_exists($subkey,self::$_configurations[$key])){
                    return self::$_configurations[$key]->{$subkey};
                }else{
                    return false;
                }
            }
            return self::$_configurations[$key];
        }
        else{
            self::register($key,self::loadFile($key));
            return self::load($key,$subkey);

        }
    }

}
