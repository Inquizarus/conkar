<?php
class Conkar_Exception_Base extends Exception
{

    public static $php_errors = array(
        E_ERROR             => 'Fatal Error',
        E_USER_ERROR        => 'User Error',
        E_PARSE             => 'Parse Error',
        E_WARNING           => 'Warning',
        E_USER_WARNING      => 'User Warning',
        E_STRICT            => 'Strict',
        E_NOTICE            => 'Notice',
        E_RECOVERABLE_ERROR => 'Recoverable Error',
        E_DEPRECATED        => 'Deprecated',
    );

    public static $error_view              = 'Error/Base';
    public static $error_view_content_type = 'text/html';

    public function __construct($message = "", array $variables = null, $code = 0, Exception $previous = null)
    {
        parent::__construct($message, (int) $code, $previous);
        $this->code = $code;
    }

    public function __toString()
    {
        return $this->text($this);
    }

    public static function handler(Exception $error)
    {
        return Conkar_Exception::html($error);
    }

    public static function html(Exception $error = null)
    {
        $view             = ($error && !empty($error::$error_view)) ? $error::$error_view : Conkar_Exception::$error_view;
        $view             = View::factory($view);
        $view->exception  = $error;
        $view->error_type = (empty(static::$php_errors[$error->code])) ? false : static::$php_errors[$error->code];
        return $view->render();
    }

    public static function text(Exception $e)
    {
        return sprintf('%s [ %s ]: %s ~ %s [ %d ]', get_class($e), $e->getCode(), strip_tags($e->getMessage()), $e->getFile(), $e->getLine());
    }

    /**
     * Fetch and HTML highlight serveral lines of a file.
     *
     * @param string $file to open
     * @param integer $number of line to highlight
     * @param integer $padding of lines on both side
     * @return string
     */

    public static function source($file, $number, $padding = 5)
    {
        // Get lines from file
        $lines = array_slice(file($file), $number - $padding - 1, $padding * 2 + 1, true);
        $html  = '<pre><code>';
        foreach ($lines as $i => $line) {
            $html .= '<b>' . sprintf('%' . mb_strlen($number + $padding) . 'd', $i + 1) . '</b> '
                . ($i + 1 == $number ? '<em>' . Conkar_Exception::h($line) . '</em>' : Conkar_Exception::h($line));
        }
        return $html . '</code></pre>';
    }

    public function getSource()
    {
        return $this->source($this->getFile(), $this->getLine());
    }

    /**
     * format code output
     *
     * @param string $code line
     * @return string
     */
    public static function h($code)
    {
        return htmlspecialchars($code, ENT_QUOTES, 'utf-8');
    }

}
