<?php
abstract class Conkar_JSONObject 
{

	public function __construct($json = false)
	{
		if ( ! empty($json) ) {
			if (is_string($json)) {
				$this->setData(json_decode($json,true));
			} elseif (is_array($json)) {
				$this->setData($json);
			}
		}
	}

	public function __get($key)
	{
		return $this->get($key);
	}

	public function get($key) 
	{
		return (isset($this->{$key})) ? $this->{$key} : false;
	}

	public function setData(array $data)
	{
		foreach ($data as $key => $value) {
			if (is_array($value)) {
				$subJson = new JSONObject;
				$subJson->setData($value);
				$this->{$key} = $subJson;
			} else {
				$this->{$key} = $value;
			}
		}
		return $this;
	}

}