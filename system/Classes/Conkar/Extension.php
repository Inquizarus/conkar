<?php
abstract class Conkar_Extension
{
    private $name        = null;
    private $description = null;
    private $version     = null;
    private $routes      = [];
    private $closures    = [];
    private $listeners   = [];

    public function routes(array $routes)
    {
        foreach ($routes as $key => $value) {
            $this->route($key, $value);
        }
    }

    public function route($name, array $settings)
    {
        $this->routes[$name] = $settings;
    }

    public function closures(array $closures)
    {
        foreach ($closures as $closure) {
            $this->closure($closure);
        }
    }

    public function closure(Closure $closure)
    {
        $this->closures[] = $closure;
    }

    public function listeners(array $listeners)
    {
        foreach ($listeners as $event => $listener) {
            $this->listener($event, $listener);
        }
    }

    public function listener($event, array $listener)
    {
        $this->listeners[$event] = $listener;
    }

    public function version($version = null)
    {
        if (is_string($version)) {
            $this->version = $version;
        }
        return $this->version;
    }

    public function name($name = null)
    {
        if (is_string($name)) {
            $this->name = $name;
        }
        return $this->name;
    }

    public function description($description = null)
    {
        if (is_string($description)) {
            $this->description = $description;
        }
        return $this->description;
    }

    public function initialize(Conkar $application)
    {
        foreach ($this->closures as $closure) {
            $closure($application);
        }

        foreach ($this->listeners as $event => $listener) {
            $application->event()->registerListener($event,$listener);
        }

        $application->routes($this->routes);


    }
}
