<?php
abstract class Conkar_Route implements Conkar_Interfaces_Route
{

    /** @var array Storage of all loaded routes */
    public static $_routes = array();

    /** @var array Route specific config */
    public $_data;

    /**
     * @param array $data Configuration data for route
     */
    public function __construct($data){
        $this->_data = $data;
    }


    /**
     * Inserts a new route in static container
     * @param string    $name Name of the route
     * @param array     $data Configuration for the route
     */
    public static function set($name,$data){
        static::$_routes[$name] = new Route($data);
    }


    /**
     * Retrieves a route from the static container by name
     * @param  string $name Route to return
     * @return Route|bool
     */
    public static function get($name){
        return array_get(static::$_routes,$name);
    }


    /**
     * Returns information from specified key in route configuration
     * @param string $name
     * @return mixed
     */
    public function getData($name){
        return array_get($this->_data,$name);
    }


    /**
     * Checks if there is a value stored in configuration
     * @param string $key
     * @return boolean
     */
    public function checkData($key){
        $data = $this->getData($key);
        return (empty($data)) ? false : true;
    }


    /**
     * Extracts parameters expected by a route uri from a uri
     * @param  [type] $route_uri [description]
     * @param  [type] $uri       [description]
     * @return [type]            [description]
     */
    public function extractParams($route_uri,$uri){
        $defaults = $this->getData('default');
        $params = array();
        $other = array();
        $i = 0;
        foreach($route_uri['path'] AS $param){
            if(strpos($param,':') === 0){ // if it is marked to be a parameter

                if(empty($uri['path'][$i])){ //
                    $params[$param] = array_get($defaults,(str_replace(':','',$param)));
                }
                else{
                    $params[$param] = $uri['path'][$i];
                }

                $other[$i] = $param;

            }
            elseif(!empty($uri['path'][$i])){
                if($param !== $uri['path'][$i]){
                    continue;
                }
                else{
                    $other[$i] = $param;
                }
            }
            $i++;
        }
        return array($params, $other);
    }


    public function compile($params,$name = false){

        $compiled = array(
            'route'      => (empty($name)) ? 'system' : $name,
            'controller' => false,
            'action'     => false,
            'params'     => array()
        );

        $check = array('controller','action');

        foreach($check AS $item){
            if($this->checkData($item)){
                $compiled[$item] = $this->getData($item);
                unset($params[':'.$item]);
            }
            elseif(isset($params[':'.$item])){
                $compiled[$item] = $params[':'.$item];
                unset($params[':'.$item]);
            }
        }

        $params = $this->reIndex($params);

        return array($params,$compiled);

    }

    public function reIndex($params){
        $keys    = array_keys($params);
        $values  = array_values($params);
        $newKeys = str_replace(':','',$keys);
        $params  = array_combine($newKeys,$values);
        return $params;
    }

    public function getUri(){
        return $this->getData('uri');
    }


    public static function getAll(){
        return Route::$_routes;
    }


    public static function match($uri, $routes){
        
        $uri            = array
        (
            'original'  => trim($uri,'/'),
            'path'      => explode('/', parse_url(trim($uri,'/'), PHP_URL_PATH))
        );

        $uri['length']  = count($uri['path']);
        $match     = false;
        $name      = false;
        $params    = array();

        foreach($routes AS $name => $route){

            if($match) break;

            $route_uri              = array();
            $route_uri['original']  = trim($route->getUri(),'/');
            $route_uri['path']      = explode('/', parse_url(trim($route->getUri(),'/'), PHP_URL_PATH));
            $route_uri['length']    = count($route_uri['path']);

            if($uri['length'] > $route_uri['length']){
                continue;
            }

           list($params,$other)  = $route->extractParams($route_uri, $uri);

           if(implode('/',$other) == $route_uri['original']){
               $match = $route;
               break;
           }
        }

        $route = ($match) ? $match : Route::get('default');

        list($params,$compiled) = $route->compile($params,$name);

        if($route->checkData('folder')){
            $compiled['folder'] = $route->getData('folder');
        }

        $compiled['params'] = $params;

        return $compiled;
    }


}
