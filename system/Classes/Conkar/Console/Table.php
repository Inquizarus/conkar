<?php
class Conkar_Console_Table implements Conkar_Console_TableInterface
{

    /**
     * Container for table headers
     * @var array
     */
    private $headers = array();
    /** @var string $string */
    private $string = '';
    /**
     * Container for table rows
     * @var array
     */
    private $rows = array();
    /**
     * Pattern to use then printing
     * @var string
     */
    private $pattern = null;
    /**
     * Determines if table headers should be adapted to length of content
     * @var boolean
     */
    private $adaptive = true;

    private $spacer = " ";

    /**
     * @param array $headers Headers to use in table
     * @param array $rows    Rows to insert in table
     */
    public function __construct(array $headers = array(), array $rows = array())
    {
        $this->headers = $headers;
        $this->rows    = $rows;
    }

    /**
     * Getter and setter for table headers
     * @param  string|array     $string     Either header label or headers array
     * @param  string|integer   $length     Length of header label
     * @return Conkar_Console_TableInterface|array
     */
    public function headers($string = null, $length = null)
    {
        if (is_array($string)) {
            $this->headers = $string;
            return $this;
        } elseif (!empty($string) && !empty($length)) {
            $this->setHeader($string, $length);
        } elseif (!empty($string)) {
            return $this->getHeader($string);
        } else {
            return $this->headers;
        }
    }

    /**
     * Setter method for table header
     * @param string    $name   Header label
     * @param integer   $length Column width
     * @return Conkar_Console_TableInterface
     */
    public function setHeader($name, $length)
    {
        $this->headers[$name] = (int) empty($length) ? strlen($name) : $length;
        return $this;
    }

    /**
     * Getter method for table header
     * @param  string   $name   Header label
     * @return integer          Header label length
     */
    public function getHeader($name)
    {
        return (int) array_get($this->headers, $name);
    }

    /**
     * Bulk getter and setter for table rows data
     * @param  array|null $rows Rows to insert in the table
     * @return Conkar_Console_TableInterface|array
     */
    public function rows(array $rows = null)
    {
        if ($rows) {
            $this->rows = $rows;
            return $this;
        } else {
            return $this->rows;
        }
    }

    /**
     * Getter and setter for single table row
     * @param  string     $key   Entity-key to store or get
     * @param  array|null $value Values to store under Entity-key
     * @return Conkar_Console_TableInterface|array
     */
    public function row($key, array $value = null)
    {
        if ($value) {
            $this->rows[$key] = $value;
            return $this;
        } else {
            return array_get($this->rows, $key);
        }
    }

    /**
     * Getter and setter for spacer string in table headers
     * @param  string $string String to use as a spacer
     * @return Conkar_Console_TableInterface|string
     */
    public function spacer($string = null)
    {
        if (is_string($string)) {
            $this->string = $string;
            return $this;
        } else {
            return $this->string;
        }
    }

    /**
     * Setter for adaptiveness of table column width
     * @param  boolean $on If columns should adapt or not
     * @return Conkar_Console_TableInterface
     */
    public function adaptive($on = true)
    {
        $this->adaptive = (boolean) $on;
        return $this;
    }

    /**
     * Getter and setter for rendering pattern
     * @param  string $pattern String pattern to use for render
     * @return Conkar_Console_TableInterface|string
     */
    public function pattern($pattern = null)
    {
        if (!empty($pattern)) {
            $this->pattern = $pattern;
            return $this;
        } else {
            return $this->pattern;
        }
    }

    protected function generatePattern()
    {
        if (empty($this->pattern())) {
            $pattern = '|';
            $headers = $this->headers();
            foreach ($headers as $label => &$length) {
                if (!empty($this->adaptive)) {
                    foreach ($this->rows() as $row) {
                        $length = (int) (strlen($row[$label]) > $length) ? strlen($row[$label]) : $length;
                    }
                }
                $pattern .= " %-" . $length . "s |";
            }
            $this->pattern($pattern);
        }
        return $this->pattern();
    }

    /**
     * Draws a table with current information accessible
     */
    public function render()
    {
        $pattern     = $this->generatePattern();
        $totalWidth  = array_sum($this->headers);
        $linebreaker = (str_repeat('-', $totalWidth + (count($this->headers()) * 3.5))) . PHP_EOL;
        echo PHP_EOL . $linebreaker;
        $this->printRow($pattern, array_keys($this->headers()));
        echo $linebreaker;
        foreach ($this->rows() as $row) {
            $this->printRow($pattern, $row);
        }
        echo $linebreaker . PHP_EOL;

    }

    public function printRow($pattern, $data)
    {
        vprintf($pattern, $data);
        echo PHP_EOL;
    }

}
