<?php
class Conkar_Console_Kernel extends ConkarObject implements Conkar_Console_KernelInterface
{

    private $application = null;
    private $command     = null;
    private $action      = null;
    private $options     = [];
    private $input       = [];
    private $table       = null;
    private $commands    = [];

    public function __construct(Conkar $application)
    {
        $application->instance('odin',$this);
        $application->run();
        $this->application($application);
        $this->loadCommands();
    }

    private function loadCommands()
    {
        $commands = array_merge(Config::loadFile('Conkar/Commands'), Config::loadFile('Commands'));
        $commands = array_merge($commands, $this->commands);
        $this->commands = $commands;
        $this->application()->event()->triggerEvent('conkar_kernel_commands_loaded', array('eventObject' => $this));
    }

    public function addCommands(array $commands = [])
    {
        foreach ($commands as $command => $class) {
                    $this->addCommand($command, $class);
                }
    }

    public function addCommand($command,$class)
    {
        $this->commands[$command] = $class;
    }

    public static function run(Conkar $application)
    {
        return new Console($application);
    }

    public function table(Conkar_Console_Table $table = null)
    {
        if ($table) {
            $this->table = $table;
        } elseif (empty($this->table)) {
            $this->table = new Conkar_Console_Table;
        }

        return $this->table;
    }

    public function bind(array $argv)
    {

        $input   = [];
        $options = [];

        if (!empty($argv[1])) {
            list($command, $action) = (strpos($argv[1], ':')) ? explode(':', $argv[1]) : [$argv[1], null];

            unset($argv[0], $argv[1]);

            foreach ($argv as $arg) {
                if (strpos($arg, '=')) {
                    list($key, $value) = explode('=', $arg);
                    $input[$key]       = $value;
                } elseif (strpos($arg, '-') !== false) {
                    $options[] = str_replace('-', '', $arg);
                } else {
                    $input[] = $arg;
                }
            }

            if (isset($input[0])) {
                $input['explicit'] = $input[0];
                unset($input[0]);
            }

        } else {
            $command = 'list';
            $action  = false;
        }

        $this->command($command);
        $this->action($action);
        $this->options($options);
        $this->input($input);

        return $this;
    }

    public function command($value = null)
    {
        if ($value) {
            $this->command = strtolower($value);
            return $this;
        } else {
            return $this->command;
        }
    }

    public function action($value = null)
    {
        if ($value) {
            $this->action = strtolower($value);
            return $this;
        } else {
            return $this->action;
        }
    }

    public function options($options = null)
    {
        if ($options) {
            $this->options = $options;
            return $this;
        } else {
            return $this->options;
        }
    }

    public function input($input = null)
    {
        if ($input) {
            $this->input = $input;
            return $this;
        } else {
            return $this->input;
        }
    }

    public function commandExist($command)
    {
        return array_key_exists($command, $this->commands);
    }

    private function runCommand($command, $options = null, $parameters = null)
    {
        try {
            if (class_exists($this->commands[$command])) {
                $options    = ($options) ? $options : $this->options();
                $parameters = ($parameters) ? $parameters : $this->input();
                $command    = new $this->commands[$command]($options, $parameters, $this);
                $command->application($this->application());
                $command->execute();
            } else {
                echo PHP_EOL . 'Command ' . $this->command . ' was found but does not work.' . PHP_EOL;
            }
        } catch (Exception $e) {
            $file = substr($e->getFile(),(strrpos($e->getFile(),"/")+1));
            $this->application()->log()->error('[Odin] Could not run command '.$command->command.' ; '.$e->getMessage() . ' in file '.$file.' row '.$e->getLine());

            echo PHP_EOL . 'Command ' . $this->command . ' could not be executed.' . PHP_EOL;
        }
    }

    public function displayInformationList()
    {
        $this->table = new Conkar_Console_Table;
        $this->table->headers(['Name' => 20, 'Description' => 50, 'Parameters' => 30, 'Options' => 30]);
        foreach ($this->commands as $command => $class) {
            $this->execute($command, ['i']);
        }
        echo 'This is a list of all commands available in the CLI.';
        $this->table->render();
    }

    public function execute($command = null, array $options = null, array $parameters = null)
    {
        $command = ($command) ? $command : $this->command() . ':' . $this->action();
        $command = ($this->commandExist($command)) ? $command : $this->command();

        if ($this->commandExist($command)) {
            $this->runCommand($command, $options, $parameters);
        } else {
            if ($command == 'list') {
                $this->displayInformationList();
            } else {
                echo PHP_EOL . $command . ' is not a Odin command' . PHP_EOL;
            }
        }

    }

}
