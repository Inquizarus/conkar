<?php
interface Conkar_Console_Commands_CommandInterface {

	/**
	 * Output message to the terminal on a new line
	 * @param  string $comment The comment
	 * @return string
	 */
	public function comment($comment);


	/**
	 * Executes the terminal command
	 */
	public function execute();

}