<?php
class Conkar_Console_Commands_GenerateController extends Conkar_Console_Commands_Generate {

			public 	$command 		= 'generate:controller',
					$description	= 'Generates a controller class',
					$commandParams  = ['Controllername'],
					$commandOptions  = ['i','plain','rest'],
					$basepath 		= 'Controller',
					$defaults 		= [
						'name' => 'Index',
						'folder' => '',
						'template' => '',
						'autorender' => 0
					];


			public function setStubByOptions(){
				if(in_array('plain', $this->options)){
					$this->stub = 'Plain';
				}elseif(in_array('rest', $this->options)){
					$this->stub = 'REST';
				}else{
					$this->stub = 'Controller';
				}
				return $this;
			}


			public function setValuesByOptionsParameters(){
				return $this->generateName()->generateFolder()->generateClassname()->setStubByOptions();
			}

			public function generateClassname(){
				$folder = array_get($this->values,'folder');
				$folder = (empty($folder)) ? DS.$folder : DS.$folder.DS;
				$classname = 'Controller'.$folder.array_get($this->values,'filename');
				$this->values['classname'] = str_replace(DS, '_', $classname);
				return $this;
			}

			public function execute(){
				parent::execute();
				if( ! in_array('i', $this->options) ) {
					$this->setValuesByOptionsParameters();
					$stub = $this->generate();
					$this->placeController($stub);
				}
			}

			public function placeController($stub){
				$folder = (empty($this->values['folder'])) ? '' : $this->values['folder'].DS;
				$file = $this->values['filename'].EXT;
				$path = 'Classes'.DS.'Controller'.DS.$folder.$file;
				$this->placeFile($path, $stub);
			}

}
