<?php
class Conkar_Console_Commands_Generate extends Conkar_Console_Commands_AbstractCommand
{

    protected $command       = 'generate';
    protected $description   = 'Generic generating command to extend when making your own';
    protected $basepath      = '';
    protected $stub          = null;
    protected $values        = [];
    protected $parameters    = [];
    protected $commandParams = ['folder', 'name'];
    protected $defaults      = ['folder' => '', 'name' => ''];

    public function loadStub($stubfile)
    {

        $path = 'Stubs' . DS . $this->basepath . DS . $stubfile . '.stub';

        $file = (is_file(APPATH . $path)) ? file_get_contents(APPATH . $path) : false;
        if (empty($file)) {
            $file = findExtFile($path, $this->application());
            $file = ($file) ? file_get_contents($file) : false;
        }
        $file = (empty($file) & is_file(SYSPATH . $path)) ? file_get_contents(SYSPATH . $path) : $file;

        return $file;
    }

    public function interpolateStub($stub)
    {
        $this->values['created'] = date('Y-m-d H:i:s');
        $values                  = $this->defaults;
        foreach ($this->parameter() as $parameter => $value) {
            $values[$parameter] = $value;
        }
        foreach ($this->values as $key => $value) {
            $values[$key] = $value;
        }
        return interpolate($stub, $values);
    }

    public function generate()
    {
        $stub = $this->loadStub($this->stub());
        return $this->interpolateStub($stub);
    }

    public function placeFile($path, $data)
    {
        $path   = str_replace(ROOT, '', APPATH . $path);
        $folder = rtrim(rtrim($path, $this->values['filename'] . EXT), DS);
        prepareDirectories($folder);
        return file_put_contents($path, $data);
    }

    public function generateName()
    {
        $name = $this->parameter('name');

        if (empty($name) && empty($this->parameter('explicit')) === false) {
            $tmp  = $this->parameter('explicit');
            $name = substr($tmp, strripos($tmp, '_'));

        } elseif (empty($name)) {
            $name = array_get($this->defaults,'name',false);
        }

        $this->values['name']     = $name;
        $this->values['filename'] = trim($name, '_');
        return $this;
    }

    public function generateFolder()
    {
        $folder = $this->parameter('folder');
        if (empty($folder) && empty($this->parameter('explicit')) === false) {
            $tmp = $this->parameter('explicit');

            if (strrpos($tmp, '_')) {
                $folder = str_replace(['_', $this->basepath . DS], [DS, ''], substr($tmp, 0, strripos($tmp, '_')));
            } else {
                $folder = $this->defaults['folder'];
            }
        }

        $this->values['folder'] = $folder;
        return $this;
    }

    public function stub($stub = null)
    {
        if (empty($stub)) {
            return $this->stub;
        }

        $this->stub = $stub;
        return $this;
    }


    /**
     * {@inheritDoc}
     */
    public function execute()
    {
        if($this->option('i')){
            parent::execute();
        }
        else{
            $this->generateFolder();
            $this->generateName();
            parent::execute();
        }
        echo PHP_EOL;
    }

}
