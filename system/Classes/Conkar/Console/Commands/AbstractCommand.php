<?php
abstract class Conkar_Console_Commands_AbstractCommand extends ConkarObject implements Conkar_Console_Commands_CommandInterface {

		private $console 		= null;
		protected $command 		= '';
		protected $description	= '';
		protected $commandOptions = ['i'];
		protected $commandParams  = [];
		protected $options 		= [];
		protected $parameters 	= [];
		protected $colors 		= [
									'black'  => '0',
									'red'    => '1',
									'green'  => '2',
									'yellow' => '3',
									'blue'   => '4',
									'purple' => '5',
									'cyan'   => '6',
									'white'  => '7',
									'modes' => [
											'regular' => 0,
											'bold' 		=> 1,
											'underline' => 4,
											'background' => false,
										]
								];


	public function __construct($options, $parameters, Console $console){
		$this->options = $options;
		$this->parameters($parameters);
		$this->console = $console;
	}

	public function commandOption($name = null, $value = null)
	{
		if (empty($name)) {
			return $this->commandOptions;
		} elseif ($name && $value) {
			$this->commandOptions[$name] = $value;
			return $this;
		}

		return empty($this->commandOptions[$name]) ? false : $this->commandOptions[$name];
	}

	public function option($name = null, $value = null)
	{
		if (empty($name)) {
			return $this->options;
		} elseif ($name && $value) {
			$this->options[$name] = $value;
			return $this;
		}

		return empty($this->options[$name]) ? false : $this->options[$name];
	}

	public function parameters( array $parameters = null)
	{
		if ($parameters) {
			$this->parameters = $parameters;
			return $this;
		}

		return $this->parameters;
	}

	public function parameter($name = null, $value = null)
	{
		if ($name && $value) {
			$this->parameters[$name] = $value;
			return $this;
		}

		if (empty($this->parameters[$name]) && in_array($name, $this->commandParam())) {
			foreach ($this->commandParam() as $key => $value) {
				if ($value == $name) {
					$name = $key;
					break;
				}
			}
		}

		return (empty($this->parameters[$name])) ? false : $this->parameters[$name];

	}

	public function commandParam($name = null, $value = null)
	{
		if (empty($name)) {
			return $this->commandParams;
		} elseif ($name && $value) {
			$this->commandParams[$name] = $value;
			return $this;
		}

		return empty($this->commandParams[$name]) ? false : $this->commandParams[$name];
	}

	/**
	 * {@inheritDoc}
	 */
	public function comment($comment){
		echo $this->colorize(PHP_EOL.$comment,'white');
	}

	/**
	 * {@inheritDoc}
	 */
	public function alert($comment){
		echo PHP_EOL.$this->colorize($comment,'red');
	}

	/**
	 * {@inheritDoc}
	 */
	public function info($comment){
		echo PHP_EOL.$this->colorize($comment,'cyan');
	}

	public function prompt($question,$exit = false){
		$handle = fopen("php://stdin","r");
	    echo $this->comment($question).': ';
	    $next_line = fgets($handle, 1024); // read the special file to get the user input from keyboard
		return ($next_line == $exit) ? $exit : trim($next_line);
	}


	public function colorize($text, $color = 'white', $mode = 'regular', $intense = false){
		$background = ($mode == 'background') ? 4 : 3;
		$mode = $this->colors['modes'][$mode];
		$colorstring = interpolate("\e[{mode};{background}{color}m",[ 'mode' => $mode, 'background' => $background, 'color' => $this->colors[$color] ]);
		return $colorstring.$text."\e[0;3".$this->colors['white']."m";
	}

	/**
	 * {@inheritDoc}
	 */
	public function execute(){
		if(in_array('i', $this->options)){
			$row = [
				'Name' 			=> $this->command,
				'Description' 	=> $this->description,
				'Parameters'	=> empty($this->commandParams) ? 'None' : '['.implode(', ', $this->commandParams).']',
				'Options'		=> empty($this->commandOptions) ? 'None' : '['.implode(', ', $this->commandOptions).']'
			];
			$this->console->table()->row(count($this->console->table()->rows()),$row);
		}
		else{
			$this->comment($this->colorize($this->colorize('Executing '.$this->command.' command','cyan'),'yellow','background'));
		}
	}




}