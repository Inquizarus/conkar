<?php
class Conkar_Console_Commands_RunTests extends Conkar_Console_Commands_AbstractCommand
{
	protected 	$command 		= 'runtests';
	protected	$description	= 'Runs PHPUnit tests';
	protected 	$commandOptions = ['i'];

	public function execute(){
		parent::execute();
		if( ! in_array('i', $this->options) ) {
			$this->info('Starting test execution.');
			$oldpath = getcwd();
			$this->info('Changing dir to system/Unit to run tests.');
			chdir(SYSPATH.'Unit');
			$this->info('Running runtests.sh');
			echo PHP_EOL;
			shell_exec('./runtests.sh');
			$this->info('Moving back to root folder.');
			chdir($oldpath);
		}
	}

}