<?php
class Conkar_Console_Commands_GenerateModel extends Conkar_Console_Commands_Generate {

			public 	$command 		= 'generate:model',
					$description	= 'Generates a model class',
					$basepath 		= 'Model',
					$commandParams  = ['Modelname'],
					$commandOptions  = ['i','pdo','api'],
					$defaults 		= [
						'folder' => '',
					];


			public function setStubByOptions(){
				if(in_array('pdo', $this->options)){
					$this->stub = 'ModelPdo';
				}else{
					$this->stub = 'Model';
				}
				return $this;
			}


			public function setValuesByOptionsParameters(){
				return $this->generateName()->generateFolder()->generateClassname()->setStubByOptions();
			}

			public function generateClassname(){
				$folder = array_get($this->values,'folder');
				$folder = (empty($folder)) ? DS.$folder : DS.$folder.DS;
				$classname = 'Model'.$folder.array_get($this->values,'filename');
				$this->values['classname'] = str_replace(DS, '_', $classname);
				return $this;
			}

			public function execute(){
				parent::execute();
				if( ! in_array('i', $this->options) ) {
					$this->setValuesByOptionsParameters();
					$stub = $this->generate();
					$this->placeModel($stub);
				}
			}

			public function placeModel($stub){
				$folder = (empty($this->values['folder'])) ? '' : $this->values['folder'].DS;
				$file = $this->values['filename'].EXT;
				$path = 'Classes'.DS.'Model'.DS.$folder.$file;
				$this->placeFile($path, $stub);
			}

}
