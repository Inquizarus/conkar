<?php
class Conkar_Console_Commands_ClearCache extends Conkar_Console_Commands_AbstractCommand
{
	protected 	$command 		= 'clear:cache';
	protected	$description	= 'Clears all cache file caches';
	protected 	$commandOptions = ['i','classmap'];

	public function execute(){
		parent::execute();
		if( ! in_array('i', $this->options) ) {
			$this->info('Clearing cache.');
			removeDirectory(STOREPATH.'cache');
			if (in_array('classmap', $this->options)) {
				$this->info('Removing classmap');
				unlink(STOREPATH.'classmap.json');
			}
		}
	}

}