<?php

abstract class Conkar_Model
{
    /**
     * Instantiates a new model object with given name.
     * @param      $name
     * @param bool $source
     * @return mixed
     */
    public static function factory($name, $source = false,Conkar $application = null)
    {
        $class = 'Model_' . ucfirst($name);
        $model = new $class($source);
        if (is_null($application) === false) {
            $application->event()->triggerEvent('conkar_model_created', array('eventObject' => $model));
        }
        return $model;
    }
}
