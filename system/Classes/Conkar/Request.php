<?php
/**
 * Request class
 */
abstract class Conkar_Request extends ConkarObject
{
    /** @var Request[] $instances Array of created Request instances */
    public static $instances = array();

    /** @var string $action What action should be run in the controller */
    private $action;

    /** @var string|Controller $Controller Either name of controller or instance of controller */
    private $controller;

    /** @var boolean $controllerExecuted Determines if controller have been run */
    private $controllerExecuted = false;

    /** @var string $folder What controller folder request points towards */
    private $folder;

    /** @var array $get GET variables from $_GET */
    private $query = [];

    /** @var string $method The HTTP_REQUEST_METHOD for Request */
    private $method;

    /** @var array $params Parameters matched from incoming URI */
    private $params = [];

    /** @var array $post POST variables from $_POST */
    private $post = [];

    /** @var string $uri URI for the request */
    private $uri;

    /**
     * @param string|boolean $uri The uri to match with routes
     * @param string|boolean $method The HTTP method to use/simulate
     */
    public function __construct($uri = false, $method = false)
    {
        if (isCli()) {
            $method = (empty($method) || !is_string($method)) ? 'GET' : $method;
        } else {
            $method = (empty($method) || !is_string($method)) ? $_SERVER['REQUEST_METHOD'] : $method;
            $uri    = (empty($uri)) ? str_replace(BASE_DIR . DS, '', $_SERVER['REQUEST_URI']) : $uri;
        }
        $this->method($method);
        $this->uri($uri);
        $this->folder(APPATH . 'Classes' . DS . 'Controller');
        $this->post($_POST);
        $this->query($_GET);
    }

    /**
     * Returns a new or already existing request based on uri
     * @param  string|boolean $uri
     * @return Conkar_Interfaces_Request
     */
    public static function factory($uri = false, $method = false)
    {
        if (isset(self::$instances[$uri])) {
            return self::$instances[$uri];
        }

        $request               = new Request($uri,$method);
        self::$instances[$uri] = $request;

        return $request;
    }


    public function execute()
    {
        $this->event('conkar_request_execute_start', array('eventObject' => $this));

        if ($this->cache() and $this->cache()->driver()->exists($this->uri())) {
            $stored = $this->cache()->load($this->uri());
            if ($stored) {
                echo $stored;
                exit();
            }
        }

        $this->initiate();

        try {
            if ($this->validController($this->controller()) === false) {
                throw new Request_Exception('Controller ' . $this->controller() . ' could not be found');
            }

            if ($this->validAction($this->action()) === false) {
                throw new Request_Exception('No action have been set!');
            }

                $controller = $this->controller();
                $response   = Response::factory();
                $response->application($this->application());
                $fpc = $response->getCacheHandler();
                $response->fpc(empty($fpc) === false);
                $controller = new $controller($this, $response);
                $controller->application($this->application());
                $controller->execute($this->action());
                $this->event('conkar_request_execute_end', array('eventObject' => $this));
                
        } catch (Exception $e) {
            $this->application()->log()->error($e->getMessage());
            echo Conkar_Exception::html($e);
            exit;
        }

    }

    /**
     * @param  string|bool $value
     * @return mixed
     */
    public function action($value = false)
    {
        return $this->interact('action', $value);
    }

    /**
     * @param  string|bool $value
     * @return $this|bool
     */
    public function controller($value = false)
    {
        return $this->interact('controller', $value);
    }

    /**
     * @param string $string
     */
    private function dirUcFirst($string, $separator = '_')
    {
        $tmp = explode($separator, $string);
        foreach ($tmp as &$x) {
            $x = ucfirst($x);
        }
        return implode($separator, $tmp);
    }

    /**
     * @param  string|bool $folder
     * @return string
     */
    public function folder($folder = false)
    {
        if (!empty($folder)) {
            if (is_string($folder)) {
                $this->folder = $folder;
            } else {
                $this->folder = APPATH . 'Classes' . DS . 'Controller';
            }
        }
        return $this->folder;
    }

    /**
     * Find and return entry from any of
     * GET/POST/PARAM inputs.
     *
     * @param  mixed Key of input to find and return
     * @return mixed
     */
    public function input($key = false)
    {
        $input = (array) $this->query();
        $input += (array) $this->post();
        $input += (array) $this->param();

        if ($key) {
            return array_get($input, $key);
        } else {
            return $input;
        }

    }

    /**
     * @return Request
     */
    public static function instance($key = false, $value = false)
    {
        if (is_array($key)) {
            static::$instances = $key;
        } elseif (!empty($key) && empty($value)) {
            return (isset(static::$instances[$key])) ? static::$instances[$key] : null;
        } else {
            static::$instances[$key] = $value;
        }
    }

    /**
     * @param  string|bool $value
     * @return string
     */
    public function method($value = false)
    {
        return $this->interact('method', $value);
    }

    /**
     * @param  string $name  Attribute name
     * @param  string|boolean  $value Attribute value
     * @return mixed
     */
    private function interact($name, $value = false)
    {
        if ($value && is_string($value)) {
            $this->{$name} = $value;
            return $this;
        } else {
            return $this->{$name};
        }
    }

    /**
     * @param  string|bool $value
     * @return mixed
     */
    public function param($key = false, $value = false)
    {
        if ($key) {
            if ($value) {
                $this->params[$key] = $value;
                return $this;
            } elseif (is_array($key)) {
                $this->params = array_merge($this->params, $key);
                return $this;
            } else {
                return array_get($this->params, $key, false);
            }
        } else {
            return $this->params;
        }
    }

    /**
     * @param  mixed $key   Index key to search for
     * @param  mixed $value Value to set to index key
     * @return self|mixed   Value from post array or current Request
     */
    public function post($key = false, $value = false)
    {
        if ($key) {
            if ($value) {
                $this->post[$key] = $value;
                return $this;
            } elseif (is_array($key)) {
                $this->post = array_merge($this->post, $key);
                return $this;
            } else {
                return array_get($this->post, $key, false);
            }
        } else {
            return $this->post;
        }
    }

    /**
     * @param  mixed $key   Index key to search for
     * @param  mixed $value Value to set to index key
     * @return self|mixed   Value from get array or current Request
     */
    public function query($key = false, $value = false)
    {
        if ($key) {
            if ($value) {
                $this->query[$key] = $value;
                return $this;
            } elseif (is_array($key)) {
                $this->query = array_merge($this->query, $key);
                return $this;
            } else {
                return array_get($this->query, $key, false);
            }
        } else {
            return $this->query;
        }
    }

    /**
     * @return string
     */
    public function uri($uri = false)
    {
        if ($uri) {
            $this->uri = (string) $uri;
            return $this;
        }

        return $this->uri;
    }

    public static function validAction($action)
    {
        return $action !== null && $action !== '';
    }

    public static function validController($controller)
    {
        if ($controller !== null && $controller !== '') {
            if (class_exists($controller)) {
                return true;
            }
        }
        return false;
    }

    private function initiate()
    {
        static $initiated = false;
        if ($initiated) {
            return $this;
        } else {
            $match      = Route::match($this->uri(), Route::getAll());
            $folder     = (empty($match['folder'])) ? '' : DS . ucfirst($match['folder']);
            $controller = (empty($match['controller'])) ? false : str_replace(array(APPATH . 'Classes' . DS, DS), array('', '_'), $this->folder() . $folder . DS . ucfirst($match['controller']));
            $action     = (empty($match['action'])) ? false : strtolower($this->method()) . ucfirst($match['action']);
            $params     = $match['params'];
            $controller = $this->dirUcFirst($controller);
            $this->controller($controller);
            $this->action($action);
            $this->param($params);
        }
    }
}
