<?php
class Conkar_Logger_Drivers_File implements Conkar_Interfaces_Logger_DriverInterface
{

    private $path               = null;
    private $file               = null;
    private $fileHandle         = null;
    private $defaultPermissions = 0777;

    private $logLevels = array(
        Conkar_Logger_Level::EMERGENCY => 'Emergency',
        Conkar_Logger_Level::ALERT     => 'Alert',
        Conkar_Logger_Level::CRITICAL  => 'Critical',
        Conkar_Logger_Level::ERROR     => 'Error',
        Conkar_Logger_Level::WARNING   => 'Warning',
        Conkar_Logger_Level::NOTICE    => 'Notice',
        Conkar_Logger_Level::INFO      => 'Info',
        Conkar_Logger_Level::DEBUG     => 'Debug',
    );

    public function __construct($path = 'logs', $file = 'base.log')
    {
        $this->path = $path . DS;
        $this->file = $file;
    }

    /**
     * {@inheritDoc}
     */
    public function write($level, $entry, $dateTime = '')
    {
        $ip = empty($_SERVER['REMOTE_ADDR']) ? 'localhost' : $_SERVER['REMOTE_ADDR'];
        $entry = '['.$dateTime.'] ['.$this->logLevels[$level].'] ['.$ip.'] '.$entry. "\r\n";
        try {
            fwrite($this->getHandle(), $entry);
        } catch (Exception $e) {
            echo $e;
        }
    }

    public function getHandle()
    {
        return ($this->fileHandle) ? $this->fileHandle : $this->openHandle();
    }

    public function openHandle()
    {
        $path = $this->path;
        $file = $this->file;

        prepareDirectories($path, $this->defaultPermissions, STOREPATH);
        $handle = fopen(STOREPATH . $path . $file, 'a+');
        if ($handle) {
            return $handle;
        } else {
            new Exception;
        }

    }

    public function closeHandle()
    {
        if ($this->getHandle()) {
            $this->getHandle()->fclose();
            chmod($this->path, $this->defaultPermissions);
        }
    }

}
