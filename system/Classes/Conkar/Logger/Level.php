<?php
/**
 * Created by PhpStorm.
 * User: conny
 * Date: 2014-11-04
 * Time: 06:58
 */ 
class Conkar_Logger_Level {
    const EMERGENCY = 0;
    const ALERT     = 1;
    const CRITICAL  = 2;
    const ERROR     = 3;
    const WARNING   = 4;
    const NOTICE    = 5;
    const INFO      = 6;
    const DEBUG     = 7;
}