<?php
/**
 * Class that enables the usage of events which let's a developer intervene the application flow
 * and modify code on the run without doing direct changes to any code.
 * When a new event container is created the currently defined eventlisteners in Settings/Events.php will get loaded.
 * Any listeners added druing runtime will get appended to the stack of specified eventkey.
 * If a listener should be sorted into it's correct position based on the position key of it's settings then
 * use the method sortListeners();
 */
  class Conkar_Event {
  /**
   * Container for currently registered listeners indexed by eventkey
   * @var array
   */
  public  $_event_container = array();
  /**
   * Specifies what file that the events should be loaded from
   * @var string
   */
  private $file;

  /**
   * Constructor should not be called directly, use class factory method or
   * Conkar::IoC->make('eventContainer');
   */
  public function __construct(){
    $this->file = 'Events';
    $this->loadListeners();
  }


  /**
   * Returns a new instance of the class Event
   * @return Event
   */
  public static function factory(){
    return new Event;
  }


  /**
   * Loads eventlisteners that are registered in the settings file Events.php
   */
   private function loadListeners(){
    $listeners = Config::loadFile($this->file);
    $this->_event_container = $listeners;
    $this->triggerEvent('eventlisteners_loaded',array('eventObject' => $this));
  }


  /**
   * Triggers an event and starts to iterate through registered listeners.
   * Good practice is to pass the object instance ($this) within the data array with
   * the key eventObject.
   * @param string $name name of triggered event
   * @param array  $data data that should be sent
   */
   public  function triggerEvent($name, $data = array('eventObject' => null)){
     $event = $this->getEvent($name);
     foreach($event AS $listener){
       if ( isset($listener['class'], $listener['method']) ) {
        $class =  new $listener['class'];
        call_user_func_array(array($class,$listener['method']),array($data));
       }
         else{
           new Exception;
         }
     }
   }


   /**
    * Registers an eventlistener that will be called when that event is triggered.
    * @param string $event name of the event to register under
    * @param array  $info  details about the listener
    */
   public  function registerListener($event, $info){
    $this->triggerEvent('eventlisteners_register',array('event' => $event,'info' => $info));
     $container = (array) $this->container();
     if(empty($container[$event])) {
       $container[$event] = array($info);
     }else{
       $container[$event][] = $info;
     }
     $this->container($container);
     return $this;
   }


   /**
    * Sorts the currently registered listeners
    */
   public  function sortListeners(){
     $listeners = $this->container();
     usort($listeners, function($a, $b) {
       return $a['priority'] - $b['priority'];
     });
     $this->container($listeners);
     return $this;
   }


   /**
    * Retrieves or sets the current registered listeners
    * @param  array $listeners
    * @return array
    */
   public function container($listeners = null){
     if($listeners){
       $this->_event_container = $listeners;
       return $this;
     }
     return $this->_event_container;
   }


   /**
    * Retrieves registered listeners for a certain event
    * @param string $event
    */
   public function getEvent($event){
     $container = $this->container();
     return (empty($container[$event])) ? array() : $container[$event];
   }

}
