<?php
abstract class Conkar_ConkarObject
{
    /** @var Conkar $application **/
    private $application = null;

    /** @var Event $eventHandler **/
    private $eventHandler = null;

    /** @var Conkar_Cache_CacheInterface $cacheHandler */
    private $cacheHandler = null;

    /** @var boolean $profilingEnabled */
    private $profilingEnabled = false;

    /*
     * @param Event $eventHandler
     *
     * @return self
     */
    public function setEventHandler(Event $eventHandler)
    {
        $this->eventHandler = $eventHandler;
        return $this;
    }

    /**
     * @return Event|NULL
     */
    public function getEventHandler()
    {
        if ($this->eventHandler) {
            return $this->eventHandler;
        }

        return $this->application()->event();
    }

    /*
     * @param Conkar_Cache_CacheInterface $cacheHandler
     *
     * @return self
     */
    public function setCacheHandler(Conkar_Cache_CacheInterface $cacheHandler = null)
    {
        $this->cacheHandler = $cacheHandler;
        return $this;
    }

    /**
     * @return Conkar_Cache_CacheInterface|NULL
     */
    public function getCacheHandler()
    {
        if ($this->cacheHandler) {
            return $this->cacheHandler;
        }

        return $this->application()->cache();
    }

    /**
     * Trigger an event with passed name and data
     *
     * @param  string $name      Name of emitted event
     * @param  mixed  $eventData What should be sent with the event
     */
    public function event($name = null, $eventData = null)
    {
        if ($this->getEventHandler()) {
            if (empty($name) && empty($eventData)) {
                return $this->getEventHandler();
            } else {
                $name = (empty($name)) ? 'common_event' : $name;
                $data = [];
                if (is_object($eventData)) {
                    $data['eventObject'] = $eventData;
                } elseif (is_array($eventData)) {
                    $data = $eventData;
                } else {
                    $data['eventData'] = $eventData;
                }
                $this->getEventHandler()->triggerEvent($name, $data);
            }
        } else {
            throw new Conkar_Exception('Could not trigger event since an eventhandler have not been registered');
        }
    }

    public function cache()
    {
        return $this->getCacheHandler();
    }

    public function profiling($value = null)
    {
        if (!is_null($value)) {
            $this->profilingEnabled = (bool) $value;
            return $this;
        }

        return $this->profilingEnabled;
    }


     /**
     * GetSetter method for class Conkar object
     * 
     * @param  Conkar|null
     * @return Conkar|Self
     */
    public function application(Conkar $application = null)
    {
      if ($application) {
        $this->application = $application;
        return $this;
      } else {
        return $this->application;
      }
    }

}
