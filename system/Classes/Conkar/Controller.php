<?php
/**
 * Class Conkar_Controller
 */
abstract class Conkar_Controller extends ConkarObject {

    /** @var bool $_get_default Determines if GET is the default method to determine action */
    public $_get_default = true;

    /** @var Conkar $application The current application bound to controller */
    public $application = null;

    /** @var Request $request The current request bound to controller */
    public $request = null;

    /** @var Response $response The current response bound to controller */
    public $response = null;

    /** @var string|View The view name or view instance that acts as a base for response */
    public $template = null;

    /** @var boolean $_auto_render Determines if view output should be automatically rendered */
    public $_auto_render = false;


    /**
    * Constructor should not be called directly, use class factory method
    * @param Request $request
    * @param Response $response
     */
    public function __construct(Request $request, Response $response)
    {

        if ($this->profiling()) {
          Profiler::start(get_class($this));
        }

        $this->request($request);
        $this->response($response);

        if ($request->getEventHandler()) {
            $this->setEventHandler($request->getEventHandler());
        }
    }


    /**
     * GetSetter method for class response object
     * 
     * @param  Response|null
     * @return Response|Self
     */
    public function response(Response $response = null)
    {
      if ($response) {
        $this->response = $response;
        return $this;
      } else {
        return $this->response;
      }
    }


    /**
     * GetSetter method for class response object
     * 
     * @param  Request|null
     * @return Request|Self
     */
    public function request(Request $request = null)
    {
      if ($request) {
        $this->request = $request;
        return $this;
      } else {
        return $this->request;
      }
    }


    /**
     * Method that is called before determined action
     */
    public function before()
    {
      $this->event('conkar_controller_before_start',array('eventObject' => $this));
        if($this->template !== Null)
        {
            $this->template = View::factory($this->template);
        }
      $this->event('conkar_controller_before_end',array('eventObject' => $this));
    }


    /**
    * Method that is called after determined action
     * Do not forget to call the parent method with parent::after()
     */
    public function after()
    {
        $this->event('conkar_controller_after_start',array('eventObject' => $this));
        
        if($this->_auto_render && $this->template !== Null)
        {
            $this->event('conkar_controller_render_body',array('eventObject' => $this));
            $body = $this->template->render();
            $this->response->body($body);
        }
        $this->event('conkar_controller_after_end',array('eventObject' => $this));
        $this->response()->send();
    }


    /**
     * Runs the action that have been determined in the requestobject earlier
     * @param string $action The name of action to run
     */
    public function execute($action)
    {
      $this->event('conkar_controller_execute_start',array('eventObject' => $this));
      $this->before();
      $this->event('conkar_controller_before_action',array('eventObject' => $this));
      
      if (method_exists($this, $action)) {
        $this->$action();
      } else {
        throw new Request_Exception('Controller action '.$action.' does not exist in controller '.get_class($this).'.');
      }

      $this->event('conkar_controller_after_action',array('eventObject' => $this));
      $this->after();
      $this->event('conkar_controller_execute_end',array('eventObject' => $this));

      if ($this->profiling()) {
        Profiler::stop(get_class($this));
      }
    }


}
