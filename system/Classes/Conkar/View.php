<?php

/**
 * View base class
 *
 * @property string $xml
 */
abstract class Conkar_View extends ConkarObject
{

    public static $globals = array();

    public $_data = array();
    public $_file;

    public function __construct($file = false)
    {
        if ($file) {
            $this->file($file);
        }
    }

    /**
     * Sets a view variable
     * @return View
     */
    public function __set($key, $value)
    {
        return $this->set($key, $value);
    }

    /**
     * Gets a stored view variable
     * @param mixed $key
     * @return mixed
     */
    public function __get($key = false)
    {
        return $this->get($key);
    }

    public function __toString()
    {
        $this->render();
    }

    public static function factory($file)
    {
        return new View($file);
    }

    /**
     * Registers variable into global view scope
     */
    public static function setGlobal($name, $data)
    {
        View::$globals[$name] = $data;
    }

    /**
     * Returns all registered global view variables
     */
    public static function getGlobals()
    {
        return static::$globals;
    }

    /**
     * Returns all registered local view variables
     */
    public function getVariables()
    {
        return $this->_data;
    }

    /**
     * Return a specific variable or a compiled array of view variables
     * based on local and global view variables.
     *
     * @param string|bool $key
     * @return array|bool
     */
    public function get($key = false)
    {
        $vars = array_merge(static::getGlobals(), $this->getVariables());
        if (!empty($key)) {

            if (!empty($vars[$key])) {
                return $vars[$key];
            } else {
                return false;
            }
        }

        return $vars;
    }

    /**
     * @param  string      $key
     * @param  string|bool $value
     * @return View        $this
     */
    public function set($key, $value = false)
    {
        if (is_array($key)) {
            foreach ($key as $name => $value) {
                $this->_data[$name] = $value;
            }
        } else {
            $this->_data[$key] = $value;
        }
        return $this;
    }

    /**
     * @param string $basepath
     */
    private function getViewFile($file, $basepath = APPATH)
    {
        $path = $basepath . 'Views' . DS . $file . EXT;
        if (file_exists($path)) {
            return $path;
        }
        return false;
    }

    /**
     * @param boolean|string $file
     */
    private function getExtensionViewFile($file)
    {
        if ($this->application()) {
            foreach ($this->application()->loadedExtensions() as $namespace => $extensions) {
                foreach ($extensions as $name => $extension) {
                    $path = EXTPATH . $namespace . DS . $extension['folder'] . DS . 'Views' . DS . $file . EXT;
                    if (file_exists($path)) {
                        return $path;
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param boolean|string $file
     */
    private function getSystemViewFile($file)
    {
        return $this->getViewFile($file, SYSPATH);
    }

    /**
     * @param bool|string $file
     * @return $this
     * @throws Exception
     */
    public function file($file = false)
    {
        if (empty($file) === false) {
            $path        = $this->getViewFile($file);
            $path        = (empty($path)) ? $this->getExtensionViewFile($file) : $path;
            $path        = (empty($path)) ? $this->getSystemViewFile($file) : $path;
            $this->_file = $path;
            return $this;
        }

        return $this->_file;
    }

    /**
     * @param  string  $file
     * @return string
     * @throws Exception
     */
    public function render($file = null)
    {
        if (!empty($file)) {
            $this->file($file);
        } else {
            if (empty($this->_file)) {
                throw new Conkar_Exception('No file have been specified to render');
            }
        }

        if (!empty($this->_data) && is_array($this->_data)) {
            foreach ($this->_data as $key => $data) {
                if ($data instanceof View) {
                    $this->_data[$key] = $data->render();
                }
            }
        }
        return $this->capture();
    }

    /**
     * @return string
     * @throws Exception
     */
    public function capture()
    {
        $vars = (is_array($this->get())) ? $this->get() : array();
        extract($vars, EXTR_SKIP);
        ob_start();
        try {
            if ($this->file() !== false) {
                include $this->file();
            }
        } catch (Exception $e) {
            ob_end_clean();
            throw $e;
        }
        return ob_get_clean();
    }

}
