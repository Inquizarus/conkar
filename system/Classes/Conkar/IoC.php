<?php
/**
 * IoC(Inversion of control) class that is used by the core functionality to easen implementation/replacing of code
 */
class Conkar_IoC implements Conkar_Interfaces_IoC
{

    /**
     * Registered resolvers
     * @var array
     */
    protected $resolvers = [];
    /**
     * Registered singleton resolvers
     * @var array
     */
    protected $singletons = [];
    /**
     * Registered instances
     * @var array
     */
    protected $instances = [];

    /**
     * Registers a new resolver that can later be called with $this->make($name, array($args));
     * @param  string  $name
     * @param  Closure $lambda
     */

    /**
     * When constructing, you can pass an assoc array to find closures from scratch
     * @param array $lambdas
     */
    public function __construct(array $lambdas = array())
    {
        foreach ($lambdas as $name => $lambda) {
            $this->register($name, $lambda);
        }
    }

    /**
     * Returns or sets an instance based on name and instance parameters
     * @param  string $name     instance name to retrieve or store
     * @param  mixed $instance  instance entity to store
     * @return mixed
     */
    public function instance($name, $instance = null)
    {
        if ($instance) {
            $this->instances[$name] = $instance;
        } else {
            return ($this->instanceExist($name)) ? $this->instances[$name] : null;
        }
    }

    /**
     * Checks if an instance with the given name is registered
     * @param  string $name instance name
     * @return bool
     */
    public function instanceExist($name)
    {
        return !empty($this->instances[$name]);
    }

    /**
     * Runs a resolver with passed params
     * @param  string $key
     * @param  array  $params
     * @return mixed
     */
    public function make($key, $params = array())
    {
        if ($this->registeredSingleton($key)) {

            if ($this->instanceExist($key)) {
                return $this->instances[$key];
            } else {
                $resolver = $this->singletons[$key];
                $instance = call_user_func_array($resolver, $params);
                $this->instance($key, $instance);
                return $this->instance($key);
            }

        } elseif ($this->instanceExist($key)) {
            return $this->instances[$key];
        }

        if ($this->registered($key)) {
            $resolver = $this->resolvers[$key];
            return call_user_func_array($resolver, $params);
        }

        throw new Exception("No registered resolver for {$key} in the container", 1);
    }

    public function register($name, Closure $lambda)
    {
        $this->resolvers[$name] = $lambda;
        return $this->registered($name);
    }

    /**
     * Checks if a resolver is registered with passed key
     * @param  string $key
     * @return bool
     */
    public function registered($key)
    {
        return array_key_exists($key, $this->resolvers);
    }

    /**
     * Checks if a singleton resolver is registered
     * @param  string $key
     * @return bool
     */
    public function registeredSingleton($key)
    {
        return array_key_exists($key, $this->singletons);
    }

    /**
     * Register a singleton instance
     *
     * @param  string  $name   Name to register closure under
     * @param  Closure $lambda Closure to register
     */
    public function singleton($name, Closure $lambda)
    {
        $this->singletons[$name] = $lambda;
    }

}
