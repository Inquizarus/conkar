#! /bin/bash

if [ -z "$1" ];
    then
    TESTPATH="Tests"
    else
    TESTPATH=$1
fi

php Bin/phpunit.phar --configuration Bin/phpunit.xml "$TESTPATH"
