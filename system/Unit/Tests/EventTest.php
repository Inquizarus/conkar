<?php
class EventTest extends PHPUnit_Framework_TestCase
{
    public function testCanBeInstantiated()
    {
        $object = new Event;
        $this->assertInstanceOf('Event', $object);
    }

    public function testListenerWorks() 
    {
    	$event = new Event;
    	$info = [
    		'priority' => 1,
    		'class' => 'PHPUnitConkarEventListener',
    		'method' => 'testEvent',

    	];
    	$event->registerListener('phpunit_testevent',$info);

    	$object =  new stdClass;
    	$object->modified = false;

    	$event->triggerEvent('phpunit_testevent',['eventObject' =>$object]);

    	$this->assertTrue($object->modified);
    }

}

class PHPUnitConkarEventListener {
	public function testEvent($eventData)
	{
		$object = array_get($eventData,'eventObject');
		$object->modified = true;
	}
}
