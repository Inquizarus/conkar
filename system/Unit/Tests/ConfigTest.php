<?php
class ConfigTest extends PHPUnit_Framework_TestCase
{
    public function testCanBeInstantiated()
    {
        $object = new Config(array());
        $this->assertInstanceOf('Config', $object);
    }

    public function testCanLoadConfigFile()
    {
    	$object = new Config(array());
    	$configFile = $object->loadFile('Application');
    	$this->assertNotEmpty($configFile, 'Config file could not be loaded!');
    }

    public function testCanStoreConfigValue()
    {
    	$config = new Config(array());
    	$expected = 'yay a config';

    	$config->set('someconfig', $expected);
    	$this->assertEquals($expected, $config->get('someconfig'));
    }

    public function testCanGetConfigValue()
    {
    	$config = new Config(array('cache' => true));
    	$this->assertNotEmpty($config->get('cache'));
    }

}
