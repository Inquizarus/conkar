<?php
class IoCTest extends PHPUnit_Framework_TestCase
{
    public function testCanBeInstantiated()
    {
        $object = new IoC;
        $this->assertInstanceOf('IoC', $object);
    }

    public function testSingletonCanBeRegisteredAndLoaded()
    {
    	$IoC = new IoC;
    	$object = new stdClass;
    	$singletonName = 'IoCTestSingletonInstance';
    	$expectedName = 'IoCTestSingletonName';
    	$object->name = $expectedName;

    	$IoC->singleton($singletonName, function() use($object) { return $object; });

    	$object = $IoC->make($singletonName);

    	$this->assertInstanceOf(stdClass::class, $object);
    	$this->assertEquals($expectedName, $object->name);
    }

}
