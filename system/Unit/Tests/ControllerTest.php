<?php
class ControllerTest extends PHPUnit_Framework_TestCase
{

    public function testCanBeInstantiated()
    {

    	$application = $this->getMockBuilder(Conkar::class)
    					->setMethods(array())
    					->getMock();

        $request = new Request(DS);
        $response = new Response('',200,[]);
        $request->application($application);
        $response->application($application);
        $controller = new Controller($request,$response);
        $this->assertInstanceOf('Controller',$controller);
    }

}
