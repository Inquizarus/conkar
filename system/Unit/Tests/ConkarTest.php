<?php
class ConkarTest extends PHPUnit_Framework_TestCase
{

    public function testCanBeInstantiated()
    {
        $application = new Conkar;
        $this->assertInstanceOf(Conkar::class,$application);
    }

    public function testItCanBeRun() 
    {
    	$application = new Conkar;
    	$application->run();

    	$this->assertInstanceOf(Event::class, $application->event());
    	$this->assertInstanceOf(Logger::class, $application->log());
    	$this->assertInstanceOf(IoC::class, $application->IoC());
    	$this->assertNotEmpty($application->getConfig());
    }
    
}
