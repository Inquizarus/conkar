<?php
class ResponseTest extends PHPUnit_Framework_TestCase
{

    public function testCanBeInstantiated()
    {
        $response = new Response('',200,[]);
        $this->assertInstanceOf('Response',$response);
    }

    public function testItReturnsABody()
    {
    	$expected = 'This is a body';
    	$response = new Response($expected,200,[]);
    	$application = new Conkar;
    	$response->application($application);
    	ob_start();
    	$response->sendBody();
    	$result = ob_get_contents();
    	ob_end_clean();
    	$this->assertEquals($expected, $result);
    }

    public function testItReturnsXmlBody()
    {
    	$expected = [
    			'root' => [
    				'persons' => [
    					'person' => [
    						'name' => 'Conny Karlsson',
    						'birthdate' => '19871018'
    					]
    				]
    			]
    		];
    	$response = Response::factory();
    	$application = new Conkar;
    	$response->application($application);
    	$response->sendXml();
    	$response->xmlBody($expected);
    	ob_start();
    	$response->sendBody();
    	$result = ob_get_contents();
    	ob_end_clean();
    	$view = View::factory('Xml');
    	$view->xml = assoc_array_xml($expected);
    	$this->assertEquals($view->render(), $result);
    }

    public function testItReturnsJsonBody()
    {
    	$expected = [
		'root' => [
			'persons' => [
				'person' => [
					'name' => 'Conny Karlsson',
					'birthdate' => '19871018'
					]
				]
			]
		];

    	$response = Response::factory();
    	$application = new Conkar;
    	$response->application($application);
    	$response->sendJson();
    	$response->jsonBody($expected);
    	ob_start();
    	$response->sendBody();
    	$result = ob_get_contents();
    	ob_end_clean();
    	$this->assertEquals(json_encode($expected), $result);
    }
}
