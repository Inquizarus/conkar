<?php
/**
 * This file contains custom functions for general use. Instead of creating helperclasses we define them
 * here. Be sure to check if a function already exist before defining.
 *
 * if ( ! function_exists([functionname]) )
 * {
 *     function [functionname]($args){...}
 * }
 *
 */

if (!function_exists('between')) {
    /* Simple helper function to get data between two strings
     * $string  The string to search in
     * $str1    The start string to find
     * $str2    The end string to find
     * usage $curl->between($curl->data, '<h1>', '</h1>');
     * However if you are parsing HTML, it's better to use a DOM library.
     */
    function between($string, $str1, $str2)
    {
        $pos = strpos($string, $str1);
        if ($pos === false) {
            return '';
        }

        $end = strpos($string, $str2, $pos + strlen($str1));

        return substr($string, $pos + strlen($str1), ($end - $pos) - strlen($str1));
    }
}

if (!function_exists('baseUrl')) {
    /**
     * Recover the base URL of the application
     * @return string
     */
    function baseUrl()
    {
        return str_replace('index.php', '', $_SERVER['SCRIPT_NAME']);
    }
}

if (!function_exists('classExist')) {
    /**
     * Checks existance of a class
     * @param $class
     * @return bool
     */
    function classExist($class)
    {
        return (class_exists($class)) ? true : false;
    }
}

if (!function_exists('getFile')) {
    /**
     * Includes a file based on the recieved path
     * @param string $path
     * @return bool
     */
    function getFile($path)
    {
        if (file_exists($path)) {
            include $path;
            return true;
        }

        return false;
    }
}

if (!function_exists('getFileContents')) {
    /**
     * Returns a files content
     * @param string $path
     * @return bool
     */
    function getFileContents($path)
    {
        if (file_exists($path)) {
            return include $path;
        }
        return false;
    }
}

if (!function_exists('methodExist')) {
    /**
     * Checks if a certain method exists in a class
     * @param $class
     * @param $method
     * @return bool
     */
    function methodExist($class, $method)
    {
        return (method_exists($class, $method)) ? true : false;
    }
}

if (!function_exists('toUpperWords')) {
    /** Convert "camelCaseWord" to "CAMEL CASE WORD"
     * @param string $string
     * @return string
     */
    function toUpperWords($string)
    {
        return trim(strtoupper(preg_replace('#(.)([A-Z]+)#', '$1 $2', $string)));
    }
}

if (!function_exists('substrTo')) {
    /**
     * Helper method that do a substro from a start index and runs until it finds the
     * first occurense of a certain character.
     * @param string $haystack
     * @param int    $start
     * @param string $end_char
     * @return mixed
     */
    function substrTo($haystack, $start = 0, $end_char = '/')
    {
        $haystack     = substr($haystack, $start);
        $end_char_pos = strpos($haystack, $end_char);
        $text         = substr_replace($haystack, '', $end_char_pos);

        return $text;
    }
}

if (!function_exists('isDirEmpty')) {
    /**
     * Checks if the supplied dir is empty or not.
     * Returns true if empty and false if files exist in it.
     * @param string $dir
     * @param bool $hidden
     **/
    function isDirEmpty($dir, $hidden = false)
    {
        $dir = rtrim($dir, "*," . DS) . DS . "*";

        if ($hidden) {
            $files = scandir($dir);
        } else {
            $files = glob($dir);
        }

        return (count($files) > 0) ? false : true;

    }
}

if (!function_exists('sanitize')) {
    /**
     * Sanitizes input from dangerous monsters
     * @param   mixed $input
     * @return  mixed
     */
    function sanitize($input)
    {
        if (is_array($input) or is_object($input)) {
            foreach ($input as $key => $val) {
                $input[$key] = sanitize($val);
            }
        } elseif (is_string($input)) {
            $input = str_replace(' ', '-', $input);
            $input = str_replace(array('å', 'ä', 'ö'), array('a', 'a', 'o'), $input);
            $input = preg_replace('/[^A-Za-z0-9\-]/', '', $input);
            if (strpos($input, "\r") !== false) {
                $input = str_replace(array("\r\n", "\r"), "\n", $input);
            }

        }
        return $input;
    }
}

if (!function_exists('printr')) {
    /**
     * Helper to format a data dump in a more readable manner
     * @param  mixed $arg
     */
    function printr($arg)
    {
        echo '<pre>';
        print_r($arg);
        echo '</pre>';
    }
}

if (!function_exists('dd')) {
    /**
     * Helper to format a data dump in a more readable manner
     * @param  mixed $args
     */
    function dd($args)
    {
        die(var_dump($args));
    }
}

if (!function_exists('array_get')) {
    /**
     * Helper to get array value by key if it exist
     * @param boolean $default
     */
    function array_get($array, $key, $default = null)
    {
        if (is_array($array) && isset($array[$key])) {
            return $array[$key];
        }
        return ($default) ? $default : false;
    }
}

if (!function_exists('array_get_recursive')) {
    /**
     * Helper to get array value by key if it exist
     */
    function array_get_recursive($array, $key)
    {
        $found = false;
        if (isset($array[$key])) {
            $found = $array[$key];
        } else {
            foreach ($array as $item) {
                if (is_array($item)) {
                    $found = array_get_recursive($item, $key);
                }
            }
        }
        return $found;
    }
}

if (!function_exists('array_get_all')) {
    /**
     * Helper to get array value by key if it exist
     */
    function array_get_all($arrays, $key)
    {
        $items = [];

        if (is_array($key)) {
            foreach ($key as $k) {
                $items = array_merge($items, array_get_all($arrays, $key));
            }
        } else {
            foreach ($arrays as $array) {
                $item = array_get($array, $key);

                if ($item) {
                    $items[] = $item;
                }
            }
            return $items;
        }
    }
}

if (!function_exists('array_get_assoc_highest')) {
    /**
     * Helper to get array value by key if it exist
     */
    function array_get_assoc_highest($arrays, $key)
    {

        $highest = 0;

        foreach ($arrays as $k => $array) {
            if (!empty($array[$key]) and $array[$key] > $highest) {
                $highest = $k;
            }
        }
        return $arrays[$highest];
    }
}

if (!function_exists('array_requires')) {
    function array_requires($input = [], $requirements = [])
    {

        $errors = [];

        foreach ($requirements as $key => $value) {
            $check = (is_string($key)) ? $key : $value;
            $type  = (is_string($key)) ? $value : false;

            if (empty($input[$check])) {
                // Key not present
                $errors[$check] = false;
            }

            if (isset($input[$check]) &&
                $type != false &&
                strtolower(gettype($input[$check])) != strtolower($type)) {
                ;
                $errors[$check] = 'Invalid type, expected ' . $type . ' but got ' . gettype($input[$check]);
            }

        }
        return $errors;
    }
}

if (!function_exists('microtime_convert')) {
    /**
     * Calculates time left until a timestamp
     */
    function microtime_convert($microtime)
    {
        $hours   = (int) ($microtime / 60 / 60);
        $minutes = (int) ($microtime / 60) - $hours * 60;
        $seconds = (int) $microtime - $hours * 60 * 60 - $minutes * 60;
        return [
            'hours'   => $hours,
            'minutes' => $minutes,
            'seconds' => $seconds,
        ];
    }
}

if (!function_exists('time_left')) {
    /**
     * Calculates time left until a timestamp
     */
    function time_left($timestamp = '')
    {
        $now      = new DateTime();
        $then     = new DateTime($timestamp);
        $interval = $now->diff($then);
        $data     = explode('#', $interval->format('%d#%H#%i#%s'));
        return [
            'days'    => $data[0],
            'hours'   => $data[1],
            'minutes' => $data[2],
            'seconds' => $data[3],
        ];
    }
}

if (!function_exists('interpolate')) {
    /**
     * Interpolates context values into the message placeholders.
     * @return string
     */
    function interpolate($message, array $context = array())
    {
        $replace = array();
        foreach ($context as $key => $val) {
            $replace['{' . $key . '}'] = $val;
        }
        return strtr($message, $replace);
    }
}

if (!function_exists('isJson')) {
    function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}

if (!function_exists('isCli')) {
    /**
     * Check if script is running with cli as an interface
     * @return boolean true if cli else false
     */
    function isCli()
    {
        if (php_sapi_name() == "cli") {
            return true;
        } else {
            return false;
        }
    }

}

if (!function_exists('prepareDirectories')) {
    /**
     * @param string $root_path
     */
    function prepareDirectories($path, $permissions = 0777, $root_path = ROOT)
    {
        $parts = explode(DS, trim($path, DS));

        foreach ($parts as $part) {
            $root_path = rtrim($root_path, DS) . DS . $part;
            if (!is_dir($root_path)) {
                mkdir($root_path, $permissions);
            }
        }
    }
}

if (!function_exists('removeDirectory')) {
    /**
     * @param string $root_path
     */
    function removeDirectory($directory)
    {
       if (is_dir($directory)) {
           $objects = scandir($directory);
           foreach ($objects as $object) {
               if ($object == '..' || $object == '.') {
                   continue;
               } elseif (is_dir($directory.DS.$object)) {
                    removeDirectory($directory.DS.$object);
               } else {
                unlink($directory.DS.$object);
               }
           }
           rmdir($directory);
       }
    }
}

if (!function_exists('assoc_array_xml')) {
    function assoc_array_xml(array $data)
    {
        $xmlString = '';
        foreach ($data as $key => $value) {
            $xmlString .= "<$key>";
            if (is_array($value)) {
                $keys = array_keys($value);
                if (isset($keys[0]) and is_string($keys[0])) {
                    $xmlString .= assoc_array_xml($value);
                } else {
                    $subkey = rtrim($key, 's');
                    foreach ($value as $subvalue) {
                        $xmlString .= "<$subkey>";
                        $xmlString .= (is_string($subvalue)) ? $subvalue : assoc_array_xml($subvalue); // check if string
                        $xmlString .= "</$subkey>";
                    }
                }
            } else {
                $xmlString .= $value;
            }
            $xmlString .= "</$key>";
        }
        return $xmlString;

    }
}

if (!function_exists('array_flatten')) {
    function array_flatten(array $array, $prefix = '.') 
    {
        $result = [];
        foreach ($array as $key => $value) {
            if (is_array($value)) {
               $result = $result + array_flatten($value,$prefix.$key.$prefix);
            } else {
                $result[$prefix.$key] = $value;
            }
        }
        $trimmedResult = [];
        array_walk($result, function($value,$key) use(&$trimmedResult) {
            $trimmedResult[ltrim($key,'.')] = $value;
        });

        return $trimmedResult;
    }
}

if (!function_exists('array_unflatten')) {
    function array_unflatten(array $array, $prefix = '.') 
    {
        $result = [];
        foreach ($array as $key => $value) {
            if (strpos($key, $prefix)) {
                $keys = explode($prefix, $key);
                printr($keys);
                $result[$keys[0]] = array_unflatten([$keys[1] =>$value]);
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    }
}

if (!function_exists('random_string')) {
    function random_string($length = 8,
        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
        $charactersLength = strlen($chars);
        $randomString     = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $chars[rand(0, $charactersLength - 1)];
        }
        return $randomString;

    }
}

if (!function_exists('frameworkSignature')) {
    function frameworkSignature()
    {
        return Conkar::CONKAR_FRAMEWORK_NAME . ' v' . Conkar::CONKAR_VERSION;
    }
}
