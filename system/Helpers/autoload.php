<?php

function saveClassMap(array $classmap = [])
{
    $filePath = STOREPATH . 'classmap.json';
    prepareDirectories(DS,0777,STOREPATH);
    file_put_contents($filePath, json_encode($classmap));
}

function getClassMap()
{
    $filePath = STOREPATH . 'classmap.json';
    if (file_exists($filePath) === false) {
        saveClassMap();
    }

    return (array) json_decode(file_get_contents($filePath));
}

/**
 * @param string|false $path
 */
function addClassToMap($class, $path)
{
    $classmap         = getClassMap();
    $classmap[$class] = $path;
    saveClassMap($classmap);
}

/**
 * @param string $class
 */
function getClass($class, Conkar $application = null)
{

    $basepath = 'Classes' . DS . $class . EXT;
    $subpaths = [
        APPATH . $basepath,
        $basepath,
        SYSPATH . $basepath,
    ];

    foreach ($subpaths as $subpath) {
        if ($subpath === $basepath && is_null($application) === false) {
            $subpath = findExtFile($basepath, $application);
            if (is_file($subpath)) {
                addClassToMap($class, $subpath);
                break;
            }
        } elseif (is_file($subpath)) {
            addClassToMap($class, $subpath);
            break;
        }
    }
    return $subpath;
}

/**
 * Tries to grab a file from within the application scope
 * @param string $file
 */
function getAppFile($file)
{
    $file = APPATH . $file;
    return getFile($file);
}

/**
 * Tries to grab a file from within the system scope
 * @param string $file
 */
function getSysFile($file)
{
    return getFile(SYSPATH . $file);
}

/**
 * @param string $class
 */
function findExtFile($class, Conkar $application)
{
    foreach ($application->loadedExtensions() as $namespace => $extensions) {
        foreach ($extensions as $name => $extension) {
            $path = EXTPATH . $namespace . DS . $extension['folder'] . DS . $class;
            if (file_exists($path)) {
                return $path;
            }
        }
    }
    return false;
}

/**
 * Main autoloader for the framework
 * @param string $class
 * @return boolean|null
 */
function autoloadClasses($class, Conkar $application = null)
{

    $class = str_replace('_', DS, $class);

    $classmap = getClassMap();

    if (isset($classmap[$class])) {
        //$path = $classmap[$class];
    }

    if (empty($path)) {
       $path = getClass($class, $application);
    }

    if ($path === false) {
        throw new Conkar_Exception("Class $class could not be found", 1);
        exit;
    }

    return getFile($path);
}

spl_autoload_register('autoloadClasses');
