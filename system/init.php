<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo frameworkSignature(); ?></title>
        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
        <style media="screen">
            body {
                background-color:rgba(200,200,200, 0.1);
                line-height: 24px;
            }

            code {
                padding:2.5px 5px;
                border:1px solid rgba(30, 30, 30, 0.5);
                background-color:rgba(255, 255, 255, 1);
                border-radius: 5px;
                display:inline-block;
            }

            .container {
                width:80%;
                margin: 0 auto;
            }

        </style>
    </head>
    <body>
        <div class="container">
            <div class="pure-g">
                <div class="pure-u-1">
                    <h1>Welcome to <?php echo frameworkSignature(); ?></h1>
                </div>
                <div class="pure-u-1">
                    <h2>It seems like there arent any controller action going on here?</h2>
                    <p>
                        To create a controller now, you got 3 different options listed below.
                    </p>
                    <div class="pure-g">
                        <div class="pure-u-1-3">
                            <p>
                                Hitting this button..
                            </p>
                            <button type="button" class="pure-button" name="button">Create Index!</button>
                        </div>
                        <div class="pure-u-1-3">
                            <p>
                                Generate a controller with the help of the frameworks CLI called odin.<br>
                                In the terminal standing in the root folder write this.
                                <code>php odin generate:controller Index</code><br>
                                And a controller named Index will be generated for you.
                            </p>
                        </div>
                        <div class="pure-u-1-3">
                            <p>
                                Follow the steps below to manually creating your first controller.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="pure-u-1">
                    <h2>Manually creating a controller</h2>
                    <p>
                        Most applications really want some controllers to make them work, unless
                        your mind is set on creating some form of wierd stuff I would really recommend
                        heading over to:
                    </p>
                    <p>
                        <code><?php echo "application".DS."Classes".DS."Controller" ?></code>
                    </p>
                    <p>
                        And create a controller.
                    </p>
                    <p>
                        A good first step would be to create a controller called Index since that controller will
                        be called per default if no controller gets called in any other way. The following example will
                        use the name Index from now on when refering to the controller class.
                    </p>
                    <p>
                        In the controller folder (shown above) create the file <code>Index.php</code> and enter the following.
                    </p>
                    <p>
                        <code>
                            class Controller_Index extends Controller { }
                        </code>
                    </p>
                    <p>
                        And that's it!<br>
                        Or is it? If you refresh this webpage now (open this page in a new tab) and you will be greeted with an error!
                    </p>
                    <p>
                        To fix this error you have to create an action, which is just the same thing as other methods in a class except that these
                        can be accessed with requests and display stuff.
                    </p>
                    <p>
                        By default a request will try access an Index method to display content. But even if you create a new method called <code>Index</code>
                        there will still be an error. You need to prefix the action with a HTTP Method (most basic ones are GET and POST) to tell the framework
                        on which premises that this method will be used. The framework can serve different versions of Index depending on the HTTP Method used.
                        The method prefix should be all lowercase (which actually is how you should name methods anyhow..).
                    </p>
                    <p>
                        Create a new method called <code>getIndex</code> and refresh the page! (In that new tab..)
                    </p>
                    <p>
                        You're now presented with a blank page. This is the bare bones of your application.
                    </p>
                    <p>
                        By default, any controller you create in the controller root folder can be accessed by passing it into the url
                        and any action created in a controller can also be accessed. Follow this pattern and everything will be dandy!
                        <code>http://<?php echo $_SERVER['SERVER_NAME'].baseUrl() ?>[controller]/[action]</code>
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>
