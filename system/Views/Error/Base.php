<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo (empty($title)) ? 'Random error' : $title ?></title>
	<style>
		body {
			background-color:rgba(250,250,250,1);
			font-family: Century Gothic, Arial, Helvetica, sans-serif;
			font-size: 18px;		
			color:white;
		}

		#main-wrapper{
			box-sizing:border-box;
			width:75%;
			margin:0 auto;
			margin-top:25vh;
			padding:50px;
			border-radius: 2px;
			background-color:rgba(10,160,170,1);
			box-shadow: 0px 5px 10px -10px rgba(30,30,30,0.3);
		}

		#main-wrapper h1{
			text-align:center;
		}

	</style>
</head>
<body>
	<div id="main-wrapper">
		<h1><?php echo (empty($heading)) ? '666 - Random error' : $heading ?></h1>
		<?php if (empty($content)): ?>
		<p>
			What just happened?
		</p>
		<?php else: ?>
			<?php echo $content ?>
		<?php endif ?>
	</div>
  </div>
</body>
</html>