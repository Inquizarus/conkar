<?php
$title = 'The page could not be found';
$heading = $title;
$content = "
		<p>
			The page you are looking for does not exist, perhaps it never have?
		</p>
		<p>
			If this page is supposed to be here, try hitting the old <em>refresh button</em> and if it does not work, please contact the site admin.
		</p>
";
require 'Base.php';