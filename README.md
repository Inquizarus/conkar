# Conkar PHP Framework 0.9.0#
***
## Information ##
Conkar is a small framework built from an idea that the best way of learning and understanding something completely is to built it yourself. Atleast once!

I would say that the framework is stable enough to use if you also care about getting your hands dirty. Things do work (it's used on my own websites. Or atleast some of them) but some optimization is needed and
structural changes can happen from time to time.

__Clone it, fork it or even stick it in a stew...__ It does not really matter to me, but if you want to help out I will always welcome good tips and critique.

## Structure ##

### Application ###
This is the applicationscope where most of all customization will be done. Classes, settings and other things. The sky is the limit.
### System ###
The frameworks heart/engine. This makes everything tick and should NEVER be tampered with. Making changes here will result in a pain when updating to a new version of Conkar and also makes debugging harder for other developers.
### Extensions ###
In here goes "reusable" functionality labeled under your own or some other developers name. By default there will exist a namespace called Inquizarus which is my own. It contains one or more extensions that are optional to enable. I decided to put it inside the extensions instead of system to prevent bloating.
### Public ###
Here be stylesheets, scripts and also images of any kind. Just a neat place to store resources that relates more to frontend things.
### Storage ###
This little treasurechest is where any dynamically created content like cache, logs or temporary uploads should land. This also exist to keep things tidy.
## Autoloading and hierarchy ##

The autoloader for Conkar is based on PSR-0 principles. Therefore all underscores in a classname will be converted to the defined directoryseparator when locating it.

The autoloading works in a heirarchy pattern and any class
that is needed will be searched for inside three places.
It will look inside **application/Classes**, if nothing is found it checks the extensions in their **[ Extensionname ]/Classes** and the last step is to check for the class in **system/Classes**.

So remember kids. The order is: **application** -> **extensions** -> **system**

## Inversion of Control (IoC) ##

Conkar have a built in container where resolvers can be registered to easen the reusal of code and keep developers from having to worry about dependencies as much. It works more or less like this.

A lambda (anonymous function) is associated with a string key which takes certain parameters and then outputs an object tooled with all dependencies needed. This acts kind of classic shake'n'bake boxes! And when an update is released you dont have to anything else, since the lambda will have been updated and you where a good 'grammer and used it! :D

To access the frameworks IoC you can use ` Conkar::IoC() ` which is where
all default resolvers are registered. You can also create your own by instantiating  the IoC class and storing it in a good place.

The IoC containers have two primary methods that is used, ` make() ` and ` register() `.

An example! Let's instantiate a controller called Controller_Index!
We tell the IoC that we want a Controller and gives it a single string wrapped in an array as parameter, which is the Controller name.

` Conkar::IoC()->make('Controller', array('Index')); `

*** MAGIC! ***

It works the same with models! Lets get us a model for Users!

` Conkar::IoC()->make('Model', array('User')); `

Aaaand! To register your own **resolver**

`   Conkar::IoC()->register('myresolver', `
`   function($myParameter, $mySecondParameter = array()) `
`   { `
`       // Do some magic logic and return something awesome. :) `
`       $myDriver = new someClass($myParameter); `
`       return new someOtherClass($myDriver, $mySecondParameter); `
`   }); `


## Altering behaviour ##
There is two primary ways of altering behaviour of the framework.

####Modifying/Replacing classes inside the application scope.
Creating a copy of a class in the system/Classes folder to the application/Classes folder will make that classfile have precedence to the one remaining in system/Classes. So any modifications made there will be applied wherever that class is used through the framework.

Any class that should replace a systemclass need to follow the same name and folderstructure, or else there wont be an effect.

An example that want to replace the eventclass

` system/Classes/Event.php `

Must have this folderstructure

`application/Classes/Event.php`


####Creating an extension that modifies/replaces classes.

This follows more or less the same procedure as the previous one. Except that this easily can be enabled/disabled and also shared with others.
The structure is a bit different since it's all wrapped in a namespace and an extensionname. Each namespace can have multiple extensions in it and is more a convinience. If we take the previous example and apply it with an extension it would look like this.

    extensions
		[namespace]/
			[extensionname]/
				[Classes]/
					Event.php
				init.php

You may notice that there is a `init.php` there, within that file you can register autoloaders, include files and such. That file will always be run when the extension is loaded.

To enable the extension you need to create a file called `Extensions.php` in the application/Settings that return an associative array of settings. The best way is to copy the file directly from the folder system/Settings and then modify it. That file also has a description of how it should be structured.

###Events
Conkar have event-functionality. This means that at certain points in runtime an event will be dispatched with data that concerns that event. This will let you catch the event and modify the data on the fly. Of course you can also dispatch your own events; It's even encouraged so that both you and other developers can change behaviour without modifying your code directly.

To dispatch an event you can use :
`Conkar::event()->dispatch(string [eventname], array [data])`

The eventname is what the listeners will hook into and the data array is what the function/method will recieve to work with.

**Systemevents will use the following structure**


    array(
        'eventObject' => object $objectToModify,
        'eventData'   => mixed $dataToModify
    )


Usally it's one or the other that is passed and not both, when creating your own events you can use any standard you want but if the purpose is to distribute the code, you should follow the system standard.

Passing a whole object into an event lets the event modify it directly while using the eventData it can be good to use a returnvalue or pass it in as a reference ( `  &$data  ` ).

To catch an event you need to register a listener. A listener is a function/method that will process the event.

To register listeners you need to create a file called `Events.php` in application/Settings and enter the information about them in an associative array. The best way is to copy the default file from system/Settings and modify that. That file also has a description of how it should be structured.
